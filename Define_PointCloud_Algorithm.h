/**
 * @file Define_PointCloud_Algorithm.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2019-06-05
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#ifndef _DEFINE_POINTCLOUD_ALGORITHM_H_
#define _DEFINE_POINTCLOUD_ALGORITHM_H_
#include <iostream>
#include <math.h>
#include <string>
#include <thread>
#include <chrono>

using std::string;
using std::wstring;

// For using Eigen types.
#include <eigen3/Eigen/Core>

#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/Dense>

// For Point Cloud Library.
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/visualization/cloud_viewer.h>

#include <pcl/filters/convolution_3d.h>
#include <pcl/filters/fast_bilateral.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/passthrough.h>

#include <pcl/registration/icp.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/region_growing.h>

//For Statistical outliner removal
#include <pcl/filters/statistical_outlier_removal.h>

//For plane extraction.
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

//For Euclidean Cluster Extraction
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

//For PCA
#include <pcl/common/pca.h>
#include <pcl/common/centroid.h>

//For Reconstruction
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>

//For Calculating moment_of_inertia and AABB.
#include <pcl/features/moment_of_inertia_estimation.h>

//For Convex Hull
#include <pcl/surface/convex_hull.h>
#include <pcl/surface/concave_hull.h>

//For recognition
#include <pcl/correspondence.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/shot_omp.h>
#include <pcl/features/board.h>
#include <pcl/recognition/cg/hough_3d.h>
#include <pcl/kdtree/kdtree_flann.h>
typedef pcl::SHOT352 DescriptorType;

// For mesh edge detection.
#include <pcl/features/organized_edge_detection.h>
#include <pcl/features/normal_3d.h>

// For triangulation and mesh construct.
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>
// For mash convention.
#include <pcl/geometry/triangle_mesh.h>
#include <pcl/geometry/quad_mesh.h>
#include <pcl/geometry/polygon_mesh.h>
#include <pcl/geometry/mesh_conversion.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

//For CUDA


#ifndef __UTILITY__
#define __UTILITY__
//#define NEED_TO_SMALL //是否需要降低点数
#define TO_RAD (3.14159265359/180.0)
#define TO_DEG (180.0/3.14159265359)
#endif

#ifndef __PCL_TYPEDEF__
#define __PCL_TYPEDEF__

typedef Eigen::Matrix4f Mat4f;
typedef Eigen::Matrix4d Mat4d;
typedef Eigen::Matrix3f Mat3f;
typedef Eigen::Matrix3d Mat3d;
typedef Eigen::Matrix<float, 6, 1> Vector6f;
typedef Eigen::Matrix<float, 7, 1> Vector7f;
typedef Eigen::Vector3f Vec3f;
typedef Vector6f Vec6f;
typedef Vector7f Vec7f;
typedef std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > stdVector_Mat4f;
typedef pcl::PointXY									PointXY;
typedef pcl::PointXYZ									PointXYZ;
typedef pcl::PointNormal								PointXYZN;
typedef pcl::PointXYZRGB								PointXYZRGB;
typedef pcl::PointXYZRGBNormal							PointXYZRGBN;
typedef pcl::PointCloud<pcl::PointXY>					PCXY;  // Point cloud type with point type : PointXY (x, y).
typedef pcl::PointCloud<pcl::PointXYZ>					PCXYZ;  // Point cloud type with point type : PointXYZ (x, y, z).
typedef pcl::PointCloud<pcl::PointNormal>				PCXYZN;  // Point cloud type with point type : PointNormal (x, y, z, nx, ny, nz, curvature).
typedef pcl::PointCloud<pcl::PointXYZRGB>				PCXYZRGB;  // Point cloud type with point type : PointXYZRGB (x, y, z, r, g, b).
typedef pcl::PointCloud<pcl::PointXYZRGBNormal>			PCXYZRGBN;  // Point cloud type with point type : PointXYZRGBNormal (x, y, z, r, g, b, nx, ny, nz, curvature).
typedef boost::shared_ptr<pcl::PointCloud<pcl::PointXY> >				PCXY_Ptr;  // pcl::PointCloud\<pcl::PointXY>::Ptr
typedef boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> >				PCXYZ_Ptr;  // pcl::PointCloud\<pcl::PointXYZ>::Ptr
typedef boost::shared_ptr<pcl::PointCloud<pcl::PointNormal> >			PCXYZN_Ptr;  // pcl::PointCloud\<pcl::PointNormal>::Ptr
typedef boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB> >			PCXYZRGB_Ptr;  // pcl::PointCloud\<pcl::PointXYZRGB>::Ptr
typedef boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGBNormal> >		PCXYZRGBN_Ptr;  // pcl::PointCloud\<pcl::PointXYZRGBNormal>::Ptr
typedef pcl::PointIndices			PCIDX;  // A set of index of a point cloud.
typedef pcl::PointIndices::Ptr		PCIDX_Ptr;  // The smart pointer type to PCIDX.
typedef pcl::PointCloud<pcl::Normal> PC_Normal;  // Only have curvature of surface of point cloud?????, but this type had been used in RecognitionModel and FindCorres_WithSHOTFeature function.
typedef pcl::PointCloud<pcl::Normal>::Ptr PC_Normal_Ptr;

// For triangulation and mesh construct.
typedef pcl::PolygonMesh PlyMesh;
typedef boost::shared_ptr<PlyMesh> PlyMesh_Ptr;
// For mesh convention.
//template<class PointT>
class MyVertexData
{
  public:
	typedef PointXYZ VertexData;
	MyVertexData (VertexData Data, const int id = -1) : id_ (id), _Data (Data) {}

	int  id () const {return (id_);}
	int& id ()       {return (id_);}

	VertexData  getData () const {return (_Data);}
	VertexData& getData ()       {return (_Data);}

  private:
	VertexData _Data;
	int id_;
};
typedef pcl::geometry::NoData NoData;
typedef pcl::geometry::DefaultMeshTraits <int   , NoData , NoData, NoData> TraitsV;
typedef pcl::geometry::DefaultMeshTraits <NoData, int    , NoData, NoData> TraitsHE;
typedef pcl::geometry::DefaultMeshTraits <NoData, NoData , int   , NoData> TraitsE;
typedef pcl::geometry::DefaultMeshTraits <NoData, NoData , NoData, int   > TraitsF;
typedef pcl::geometry::DefaultMeshTraits <int   , int    , int   , int   > TraitsAD;
typedef pcl::geometry::DefaultMeshTraits<pcl::PointXYZ, PointXYZ , PointXYZ, PointXYZ> MyTraits;
//MyVertexData::VertexData b;
typedef pcl::geometry::PolygonMesh <TraitsV>  MeshV;
typedef pcl::geometry::PolygonMesh <TraitsHE> MeshHE;
typedef pcl::geometry::PolygonMesh <TraitsE>  MeshE;
typedef pcl::geometry::PolygonMesh <TraitsF>  MeshF;
typedef pcl::geometry::PolygonMesh <TraitsAD> MeshAD;
typedef pcl::geometry::PolygonMesh <MyTraits> MyMeshType;
//MyMeshType::VertexDataCloud b;

#endif

#endif
