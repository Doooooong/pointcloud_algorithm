/**
 * @file Functions.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2019-06-05
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#ifndef _FUNCTIONS_H_
#define _FUNCTIONS_H_

#include "Define_PointCloud_Algorithm.h"
#include "ConsoleColor.h"


#if _GLIBCXX_CHRONO
#define START_TIMER(NAME) auto NAME = std::chrono::high_resolution_clock::now()
#define STOP_TIMER(NAME) std::cout << "Timer [" << #NAME << "] use " << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - NAME).count() << " microseconds." << std::endl
#endif

/**
 * @brief The settings for Euclidean Clustering.
 */
typedef struct EuclideanClusterSettingStruct
{
	float ClusterTolerance = 0.01f;  ///< The distance threashold between two clusters.
	uint32_t MinClusterSize;  ///< Min number of points in result clusters.
	uint32_t MaxClusterSize;  ///< Max number of points in result clusters. The number of points$p$ in final result clusters is [MinClusterSize \f$<p<\f$ MaxClusterSize].
}EC_Setting;

typedef struct RegionGrowCLusterSettingStruct
{
	float		SmoothnessThreshold = (10.0 * TO_RAD);
	float		CurvatureThreshold = (1.0);
	int			NormalKSerach = 50;
	int			NumberOfNeighbours = 30;
	uint32_t	MinClusterSize;
	uint32_t	MaxClusterSize;
}RGC_Setting;

// Const type variables for Filters() function.
const int Filter_FastBilateral = 0x01;  // FastBilateralFilter
const int Filter_Voxel = 0x02;  // VoxelGrid
const int Filter_Pass = 0x04;  // PassFilter
const int Filter_Stat = 0x08;  // StatisticRemover

/**
 * @brief 
 */
typedef struct FiltersSettingStruct
{
	std::string FilterFieldName = "z";
	float FilterLimitsMin = 0.0f;
	float FilterLimitsMax = 1.0f;

	float LeafSizeX = 0.05f;
	float LeafSizeY = 0.05f;
	float LeafSizeZ = 0.05f;

	uint32_t MeanK = 50;
	float StddevMulThresh = 0.6f;

	float SigmaS = 1;
	float SigmaR = 0.02;
}FiltersSetting;

typedef struct RecognizeSettingStruct
{
	uint32_t NormalKSearch = 15;

	bool BOARD_RF_FindHole = true;
	float BOARD_RF_RadiusSearch = 0.015f;

	float HoughBinSize = 0.01f;
	float HoughThreshold = 5.0f;
	bool HoughUseInterpolation = true;
	bool HoughUseDistanceWeight = false;
}RegnSetting;

typedef struct ICP_SettingStruct
{
	double MaxCorrespondenceDistance = 1;
	double TransformationEpsilon = 1e-10;
	double EuclideanFitnessEpsilon = 10e-6;
	int MaximumIterations = 100;
	double ScoreLimit = 10e-6;

	double AngleLimit = 5;
	double TranLimit = 0.05;
}ICP_Setting;

typedef struct PCAOutStruct
{
	Mat4f TransformMatrix;
	PCXYZ_Ptr ProjectPC;
	Eigen::Matrix3f Rotation;
	Eigen::Vector3f Transform;
	Vector6f xyzabc;
}PCAOut;

typedef struct PC_SliceStruct
{
	float StartHeight = 0.f;
	float Thickness = 0.f;
	PCXYZ_Ptr Sliced_PC = PCXYZ_Ptr(new PCXYZ);
}PC_Slice;

template<typename T1>
inline void Std_Out(T1 && Data)
{
	std::cout << Data << std::endl;
}
/**************************************
 * @brief: 
 * @param[in]:	T1 & & Var1 :
 * @param[in]:	T & & ... args :
 * @return:		void
 * @note: 
 * @author: Dong
 * @date: 2019/06/04-20:53:30
 **************************************/
template<typename T1, typename ... T>
inline void Std_Out(T1 && Var1, T && ... args)
{
	std::cout << Var1;
	int NoUse_OnlyForOutput[] = { (std::cout << args, 0)..., 0 };
	std::cout << std::endl;
}
template<typename T1>
inline void Std_WRN_Out(T1 && Data)
{
	std::cout << __COLOR_BOLDYELLOW__ << Data << __COLOREND__ << std::endl;
}
template<typename T1, typename ... T>
inline void Std_WRN_Out(T1 && Var1, T && ... args)
{
	std::cout << __COLOR_BOLDYELLOW__ << Var1;
	int NoUse_OnlyForOutput[] = { (std::cout << args, 0)..., 0 };
	std::cout << __COLOREND__ << std::endl;
}
template<typename T1>
inline void Std_ERR_Out(T1 && Data)
{
	std::cout << __COLOR_BOLDRED__ << Data << __COLOREND__ << std::endl;
}
template<typename T1, typename ... T>
inline void Std_ERR_Out(T1 && Var1, T && ... args)
{
	std::cout << __COLOR_BOLDRED__ << Var1;
	int NoUse_OnlyForOutput[] = { (std::cout << args, 0)..., 0 };
	std::cout << __COLOREND__ << std::endl;
}

void CovertTo_OrgnizedPointCloud(PCXYZ_Ptr &Source, double Width, double Height);
void CovertTo_UnOrgnizedPointCloud(PCXYZ_Ptr &Source);
Eigen::Matrix4f MakeTransformMatrix(double Data[4][4]);
Eigen::Matrix4f MakeTransformMatrix(float Data[4][4]);

/**
 * @brief Apply four basic filters to point cloud, for point cloud pre-processing.\r
 * @caution: This is old function, do not use.
 * @deprecated
 * 
 * @param Source 
 * @param Output 
 * @return PCXYZ_Ptr 
 */
PCXYZ_Ptr Filters(PCXYZ_Ptr Source, PCXYZ_Ptr &Output);
/**
 * @brief Apply four basic filters to point cloud, for point cloud pre-processing.
 * 
 * @param Source : Point cloud pointer that want to process.
 * @param Output : Result point cloud pointer.
 * @param Setting : Settings for each filter. @link FiltersSetting
 * @param FilterMask : Mask for specify which filter will be applied.
 * @return PCXYZ_Ptr Result point cloud pointer .
 * 
 * @sa: <a href="http://pointclouds.org/documentation/tutorials/voxel_grid.php#voxelgrid">Voxel Grid Filter</a>, 
 * <a href="http://pointclouds.org/documentation/tutorials/passthrough.php#passthrough">Pass Through Filter</a>, 
 * <a href="http://pointclouds.org/documentation/tutorials/statistical_outlier.php#statistical-outlier-removal">Statistical Outlier Removal</a>, 
 * <a href="http://docs.pointclouds.org/trunk/classpcl_1_1_bilateral_filter.html">Bilatera Filter</a>, 
 * 
 * @caution: BilateraFilter need struct point cloud(point cloud that storaged like 2-D depth image), we can not get struct point cloud, so this filter is useless.
 * 
 * @code:
 * Filters(read, result, filter_setting, Filter_Voxel | Filter_Pass | Filter_Stat);  // Apply only [VoxelGrid],[PassThrough],[StatisticalOutlierRemoval] filters.
 * @endcode
 */
PCXYZ_Ptr Filters(PCXYZ_Ptr Source, PCXYZ_Ptr &Output, FiltersSetting Setting, uint8_t FilterMask=0x0E);
Vector6f ExtractPlane(PCXYZ_Ptr Source, PCXYZ_Ptr Plane = PCXYZ_Ptr(new PCXYZ), PCXYZ_Ptr Rest = PCXYZ_Ptr(new PCXYZ), float ThreadHold = 0.004, int MinPoints = 0);
Vector6f ExtractPlane(PCXYZ_Ptr Source, PCXYZ_Ptr Plane = PCXYZ_Ptr(new PCXYZ), PCXYZ_Ptr Rest = PCXYZ_Ptr(new PCXYZ), float ThreadHold = 0.004, double Percent = 0.1);
Vector6f ExtractParallelPlane(PCXYZ_Ptr Source, Vec3f Axis, PCXYZ_Ptr Plane = PCXYZ_Ptr(new PCXYZ), PCXYZ_Ptr Rest = PCXYZ_Ptr(new PCXYZ), float ThreadHold = 0.004, int MinPoints = 0);
Vector7f ExtractCylinder(PCXYZ_Ptr Source, PCXYZ_Ptr Plane = PCXYZ_Ptr(new PCXYZ), PCXYZ_Ptr Rest = PCXYZ_Ptr(new PCXYZ), float ThreadHold = 0.004, double Percent = 0.1);
Vector7f ExtractCircle3D(PCXYZ_Ptr Source, PCXYZ_Ptr Circle, PCXYZ_Ptr Rest, float ThreadHold = 0.004f, double Percent = 0.1);

enum ObjectType
{
	Cube,
	Cylinder,
	Other
};

PCXYZ_Ptr ReconstructSurface(PCXYZ_Ptr Source, float SearchRadius = 0.010);
PCXYZ_Ptr ReconstructSurface_FillHole(PCXYZ_Ptr Source, float SearchRadius = 0.010);
int ExtractEuclideanCluster(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[]);
int ExtractEuclideanCluster(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[], EC_Setting Setting, bool ClusterWithoutZAxis = false);
int ExtractRegionGrowCLuster(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[], RGC_Setting Setting);
int RecognitionModel(PCXYZ_Ptr Model, PCXYZ_Ptr Scene, stdVector_Mat4f &Tran, std::vector<pcl::Correspondences> &clustered_corrs, RegnSetting Setting);
Mat4f ICP_Single(PCXYZ_Ptr Source, PCXYZ_Ptr Target, PCXYZ_Ptr Output, ICP_Setting Setting);
Mat4f ICP_Single(PCXYZ_Ptr Source, PCXYZ_Ptr Target, PCXYZ_Ptr Output, double ScoreLimit = 10e-6);
Mat4f ICP_Single_Limit(PCXYZ_Ptr Source, PCXYZ_Ptr Target, PCXYZ_Ptr &Output, ICP_Setting Setting);

void Change_PCA_CoordinateOrder_CoordinateType(Mat4f &Data, int Priciple1Axis, int Priciple2Axis, bool isRightHandCoordinate, float SecondPrincpleAngle = 999.0f);
PCAOut PCA_Cal(PCXYZ_Ptr Source, std::string CoordinateOrder = "+x+y", bool NeedProjection = false, bool isRightHandCoordinate = true);
PCXYZ_Ptr CompressPC(PCXYZ_Ptr Source, std::string AxisReserve = "xy", float Pos1 = 0.0f, float Pos2 = 0.0f);
void Cal_AABB(PCXYZ_Ptr Source, PointXYZ& min, PointXYZ& max);
Vec6f Cal_AABB(PCXYZ_Ptr Source);
void Get_minPoint(PCXYZ_Ptr Source, PointXYZ& min, std::string axis = "z");
Eigen::Vector4f Analyze_Dir_Center(PCXYZ_Ptr Source, Mat4f PCA_Result_Matrix);

Mat3f CalRotationMatrix_byDirectionVec(Eigen::Vector3f Dir, Eigen::Vector3f Ref = Eigen::Vector3f::UnitZ());

void RemovePoints_byModelAABB(PCXYZ_Ptr Source, PCXYZ_Ptr Model, PCXYZ_Ptr Output, double Scale = 1.0);
void RemovePoints_byAABB(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max, PCXYZ_Ptr Output, double Scale = 1.0);
void RemovePoints_byNeartest(PCXYZ_Ptr Source, PCXYZ_Ptr Model, PCXYZ_Ptr OutPut, PCXYZ_Ptr OutPut_Neg = PCXYZ_Ptr(new PCXYZ), double radius = 0.01);
std::vector<float> GetNearPoints_Radius(PCXYZ_Ptr Source, PointXYZ Point, PCXYZ_Ptr &OutPut, double radius = 0.01, bool NeedRest = false, PCXYZ_Ptr OutPut_Neg = PCXYZ_Ptr(new PCXYZ));
std::vector<float> GetNearPoints_NearestNum(PCXYZ_Ptr Source, PointXYZ Point, PCXYZ_Ptr &OutPut, int NearestNum = 1, bool NeedRest = false, PCXYZ_Ptr OutPut_Neg = PCXYZ_Ptr(new PCXYZ));
void GetPoints_Near2PointsLine(PCXYZ_Ptr Source, PointXYZ P1, PointXYZ P2, PCXYZ_Ptr &OutPut, double radius = 0.004, double delta = 0.0005);

PlyMesh_Ptr CreatePlyMesh(PCXYZ_Ptr Data);

int SlicePointCloud(PCXYZ_Ptr Data, PCXYZ_Ptr SlicedPC[], PC_Slice res[], float Thickness = 0.002f, int MinPoints = 20);
int SlicePointCloud2(PCXYZ_Ptr Data, PCXYZ_Ptr SlicedPC[], PC_Slice res[], float Thickness = 0.002f, int MinPoints = 20, float CutoffHeight = INFINITY);
double VolumeCalculation_byZAxisSlice(PCXYZ_Ptr Model, Vec6f AABB = Vec6f::Zero(), float LayerHeight = 0.001f);

PCXYZ_Ptr TransformPC_viaPoint(PCXYZ_Ptr Data, Vec3f Point, Eigen::Affine3f Rotation);
PCXYZ_Ptr TransformPC_viaPoint(PCXYZ_Ptr Data, PointXYZ Point, Eigen::Affine3f Rotation);

PCXYZ_Ptr Extract_FromIndices(PCXYZ_Ptr Data, PCIDX_Ptr idx);

Vec6f Cvt_TransformMatrix2xyzabc(Mat4f Data, int RotationOrd1 = 0, int RotationOrd2 = 1, int RotationOrd3 = 2);
PCXYZ_Ptr Cvt_PCXYZN_To_PCXYZ(PCXYZN_Ptr Source);

float MeanFilter(int Size, float NewData);

PointXYZ ProjectPoint2Line(PointXYZ P1, PointXYZ P2, PointXYZ ProjectPoint, float &Percent);

/// Inline functions block.
#ifndef _POINTXYZ_OPERATORS_
#define _POINTXYZ_OPERATORS_
inline PointXYZ operator + (PointXYZ l, PointXYZ r)
{
	PointXYZ res;
	res.x = l.x + r.x;
	res.y = l.y + r.y;
	res.z = l.z + r.z;
	return res;
}
inline PointXYZ operator - (PointXYZ l, PointXYZ r)
{
	PointXYZ res;
	res.x = l.x - r.x;
	res.y = l.y - r.y;
	res.z = l.z - r.z;
	return res;
}
template<typename T>
inline PointXYZ operator / (PointXYZ l, T r)
{
	PointXYZ res;
	res.x = l.x / (float)r;
	res.y = l.y / (float)r;
	res.z = l.z / (float)r;
	return res;
}
inline float operator * (PointXYZ l, PointXYZ r)
{
	return l.x * r.x + l.y * r.y + l.z * r.z;
}
template<typename T>
inline PointXYZ operator * (PointXYZ l, T r)
{
	PointXYZ res;
	res.x = l.x * (float)r;
	res.y = l.y * (float)r;
	res.z = l.z * (float)r;
	return res;
}
inline bool operator == (PointXYZ l, PointXYZ r)
{
	PointXYZ res = l - r;
	if ( ( abs(res.x) + abs(res.y) + abs(res.z) ) < 1e-8 )
		return true;
	else
		return false;
}
inline bool operator != (PointXYZ l, PointXYZ r)
{
	PointXYZ res = l - r;
	if ( ( abs(res.x) + abs(res.y) + abs(res.z) ) > 1e-8 )
		return true;
	else
		return false;
}
#endif
inline bool operator == (Vec3f l, Vec3f r)
{
	Vec3f res = l - r;
	if ( ( abs(res(0)) + abs(res(1)) + abs(res(2)) ) < 1e-8 )
		return true;
	else
		return false;
}

inline Vec3f Cvt_PointXYZ2Vec3f (PointXYZ l)
{
	Vec3f res(l.x, l.y, l.z);
	return res;
}
inline PointXYZ Cvt_Vec3f2PointXYZ(Vec3f l)
{
	PointXYZ res(l(0), l(1), l(2));
	return res;
}
inline float CalVector3Angle(Vec3f data1, Vec3f data2)
{
	return data1.dot(data2) / (data1.dot(data1) * data2.dot(data2));
}
inline float CalDistance(Vec3f p1, Vec3f p2, bool is2D=false)
{
	if (!is2D)
		return std::sqrt(std::pow(p1[0] - p2[0], 2) + std::pow(p1[1] - p2[1], 2) + std::pow(p1[2] - p2[2], 2));
	else
		return std::sqrt(std::pow(p1[0] - p2[0], 2) + std::pow(p1[1] - p2[1], 2));
}
inline float CalDistance(PointXYZ p1, PointXYZ p2, bool is2D=false)
{
	if (!is2D)
		return std::sqrt(std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2) + std::pow(p1.z - p2.z, 2));
	else
		return std::sqrt(std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2));
}
inline float CalDistance(PointXY p1, PointXY p2, bool is2D=false)
{
	return std::sqrt(std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2));
}

//#define __NEED_READ_FROM_FILE__
#ifdef __NEED_READ_FROM_FILE__
#define NUM_OF_DATA 19
// 读取文件夹内的点云并合成，转移矩阵存于同一个文件夹。
bool ReadPointCloudData(std::string Path, PCXYZ_Ptr Output);
#endif

#endif // !_FUNCTIONS_H_

