/**
 * @file Functions.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2019-06-05
 * 
 * @mainpage
 * Before you using this library
 * Arranged by frequency of using.
 * # Functions Introduction
 * ## Filter
 * #MakeTransformMatrix (double)
 * ## Feature Extractor
 * ## PCA
 * ## Neighbor Finding
 * ## ICP
 * ## Point Cloud Operation
 * #MakeTransformMatrix("asdasd")
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#include "Functions.h"
#include "Visualizer.h"
#include <queue>
#include <mutex>

/**
 * @brief Convent 2-D array to Mat4f.
 * 
 * @param double Data[4][4] : double array.
 * @return Mat4f : Convented matrix.
 */
Mat4f MakeTransformMatrix(double Data[4][4])
{
	Mat4f tmp;
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			tmp(i, j) = Data[i][j];
	return tmp;
}
/**
 * @brief Convent 2-D array to Mat4f.
 * 
 * @param float Data[4][4] : float array.
 * @return Mat4f : Convented matrix.
 */
Mat4f MakeTransformMatrix(float Data[4][4])
{
	Mat4f tmp;
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			tmp(i, j) = Data[i][j];
	return tmp;
}


/**
 * @brief Old preprocess filter function. Now not use.
 * 
 * @param Source 
 * @param Output 
 * @return PCXYZ_Ptr 
 */
PCXYZ_Ptr Filters(PCXYZ_Ptr Source, PCXYZ_Ptr &Output)
{
	//////// Settings of PassFilter
	pcl::PassThrough<pcl::PointXYZ> PassFilter;
	PassFilter.setFilterFieldName("z");
	PassFilter.setFilterLimits(-0.0, 0.30);  // 0.30

	//////// Settings of VoxelGridFilter
	pcl::VoxelGrid<pcl::PointXYZ> VoxelGrid_sor;
	VoxelGrid_sor.setLeafSize(0.001f, 0.001f, 0.001f);//Set the size of VoxelGrid.

	//////// Settings of BilateraFilter
	pcl::FastBilateralFilter<pcl::PointXYZ> fbf;
	fbf.setSigmaS(1);
	fbf.setSigmaR(0.02);

	//////// Settings of StatisticalOutlierRemover
	pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
	sor.setMeanK(50);//50
	sor.setStddevMulThresh(0.6);

	//////// Temporary variables for this function.
	PCXYZ_Ptr tmp(new PCXYZ);
	PCXYZ_Ptr tmp2(new PCXYZ);
	PCXYZ_Ptr tmp3(new PCXYZ);
	PCXYZ_Ptr tmp4(new PCXYZ);

	//////// Do the FastBilateralFilter.
//	CovertTo_OrgnizedPointCloud(Source, 640, 480);
//	fbf.setInputCloud(Source);
//	fbf.filter(*tmp);
//	CovertTo_UnOrgnizedPointCloud(tmp);
//	std::cout << (*tmp).size() << std::endl;

	//////// Do the VoxelGrid filter.
	VoxelGrid_sor.setInputCloud(Source);
	VoxelGrid_sor.filter(*tmp3);
	//std::cout << (*tmp2).size() << std::endl;

	//////// Do Extract Plane.
	//ExtractPlane(tmp2,tmp,tmp3,0.012);

	//////// Do the Pass filter.
	PassFilter.setInputCloud(tmp3);
	PassFilter.filter(*tmp2);

	//std::cout << (*tmp3).size() << std::endl;
	//////// Do the
//	sor.setInputCloud(tmp2);
//	sor.filter(*tmp);
	sor.setInputCloud(tmp2);
	sor.filter(*Output);

	return Output;
}


/**
 * @brief Filters for preprocessing. 
 * @par Usage: 
 * 1. Prepare input and output point cloud pointer. 
 * 2. Make a instance of {@link FilterSetting} struct, and setup parameters you need. 
 * 3. Using "|" operator to combine the filter specify bits specific filters you want.
 * 	3.1 Filter specify bits: {@link Filter_FastBilateral}, {@link Filter_Voxel}, {@link Filter_Pass}, {@link Filter_Stat}.
 * @code
 * FiltersSetting fSetting;
 * {
		fSetting.FilterFieldName = "z";
		fSetting.FilterLimitsMin = 0.0f;
		fSetting.FilterLimitsMax = 1.0f;

		fSetting.LeafSizeX = 0.05f;
		fSetting.LeafSizeY = 0.05f;
		fSetting.LeafSizeZ = 0.05f;

		fSetting.MeanK = 50;
		fSetting.StddevMulThresh = 0.6f;

		fSetting.SigmaS = 1;
		fSetting.SigmaR = 0.02;
	}
	// Use all four filters.
	PCXYZ_Ptr ret = Filters(Source, ret2, fSetting, Filter_FastBilateral | Filter_Voxel | Filter_Pass | Filter_Stat);
	// Use filters without FastBilateral filter.
	ret = Filters(Source, ret2, fSetting, Filter_Voxel | Filter_Pass | Filter_Stat);
 * @endcode
 * 
 * @note {@link FilterSetting} has default value.
 * 
 * @param Source Oringin point cloud pointer.
 * @param Output Output point cloud pointer.
 * @param Setting {@link FilterSetting} type setup struct for each filter.
 * @param FilterMask Decided which filter will be used.
 * @return PCXYZ_Ptr Output point cloud pointer.
 */
PCXYZ_Ptr Filters(PCXYZ_Ptr Source, PCXYZ_Ptr &Output, FiltersSetting Setting, uint8_t FilterMask)
{
	//////// Settings of PassFilter
	pcl::PassThrough<pcl::PointXYZ> PassFilter;
	PassFilter.setFilterFieldName(Setting.FilterFieldName);
	PassFilter.setFilterLimits(Setting.FilterLimitsMin, Setting.FilterLimitsMax);  // 0.30

	//////// Settings of VoxelGridFilter
	pcl::VoxelGrid<pcl::PointXYZ> VoxelGrid_sor;
	VoxelGrid_sor.setLeafSize(Setting.LeafSizeX, Setting.LeafSizeY, Setting.LeafSizeZ);//Set the size of VoxelGrid.

	//////// Settings of BilateraFilter
	pcl::FastBilateralFilter<pcl::PointXYZ> fbf;
	fbf.setSigmaS(Setting.SigmaS);
	fbf.setSigmaR(Setting.SigmaR);

	//////// Settings of StatisticalOutlierRemover
	pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
	sor.setMeanK(Setting.MeanK);//50
	sor.setStddevMulThresh(Setting.StddevMulThresh);

	//////// Temporary variables for this function.
	PCXYZ_Ptr tmp(new PCXYZ);
	PCXYZ_Ptr tmp1(new PCXYZ);
	PCXYZ_Ptr tmp2(new PCXYZ);
//	PCXYZ_Ptr tmp4(new PCXYZ);

	//////// Do the FastBilateralFilter.
	if ((FilterMask & 0x01) == 0x01)
	{
		CovertTo_OrgnizedPointCloud(Source, 640, 480);
		fbf.setInputCloud(Source);
		fbf.filter(*tmp);
		CovertTo_UnOrgnizedPointCloud(tmp);
	}
	else
		tmp = Source;

	//////// Do the VoxelGrid filter.
	if ((FilterMask & 0x02) == 0x02)
	{
		VoxelGrid_sor.setInputCloud(tmp);
		VoxelGrid_sor.filter(*tmp1);
	}
	else
		tmp1 = tmp;

	//////// Do the Pass filter.
	if ((FilterMask & 0x04) == 0x04)
	{
		PassFilter.setInputCloud(tmp1);
		PassFilter.filter(*tmp2);
	}
	else
		tmp2 = tmp1;

	//////// Do the
	if ((FilterMask & 0x08) == 0x08)
	{
		sor.setInputCloud(tmp2);
		sor.filter(*Output);
	}
	else
		Output = tmp2;
	return Output;
}


/**
 * @brief This function is deprecated.
 * @deprecated
 * // 转换至结构化点云-即有长跟宽的点云
	// Para-1:PCXYZ_Ptr &Source PointCloud that you want to convert.
	// Para-2.3: width and height of pointcloud you want.
	// note: If points of pointcloud is not equal to Width * Height,
 * @param Source This function is deprecated.
 * @param Width This function is deprecated.
 * @param Height This function is deprecated.
 */
void CovertTo_OrgnizedPointCloud(PCXYZ_Ptr &Source, double Width, double Height)
{
	double Scale = Width / Height;
	double RealHeight = sqrt((*Source).size() / Scale);

	(*Source).height = round(RealHeight);// 注意，不能让新算的总点数超过真实点数。
	(*Source).width = RealHeight * Scale;
	if ((*Source).height * (*Source).width > (*Source).size())// 如果新算的大小大于原始的，则将宽度减少1；
		(*Source).width--;
}
void CovertTo_UnOrgnizedPointCloud(PCXYZ_Ptr &Source)
{
	(*Source).height = 1;
	(*Source).width = (*Source).size();
}


// ExtractPlane: Points limit version
/**
 * @brief Extract the plane with max number of points. The point number of extracted plane must larger than [MinPoints]
 * 
 * @param Source Oringin point cloud pointer.
 * @param Plane The plane extracted.
 * @param Rest The rest of points.
 * @param ThreadHold The thinkness of plane.
 * @param MinPoints The point number of extracted plane must be larger than [MinPoints]. If not, return empty or zeros.
 * @return Vector6f [Center point x, Center point y, Center point z, Normal x, Normal y, Normal z]
 */
Vector6f ExtractPlane(PCXYZ_Ptr Source, PCXYZ_Ptr Plane, PCXYZ_Ptr Rest, float ThreadHold, int MinPoints)//抽出平面，Rest去除平面后的点云；返回值是抽出的平面。
{
	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
	// Create the segmentation object
	pcl::SACSegmentation<pcl::PointXYZ> seg;
	// Optional
	seg.setOptimizeCoefficients(true);
	// Mandatory
	seg.setModelType(pcl::SACMODEL_PLANE);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setDistanceThreshold(ThreadHold);//0.02

	seg.setInputCloud(Source);
	seg.segment(*inliers, *coefficients);

	if (inliers->indices.size() <= (unsigned int)MinPoints)// 控制最小点数
	{
		PCL_ERROR("ExtractPlane(): Could not estimate a planar model for the given dataset.");
		Plane->clear();
		Rest->clear();
		return Vector6f().setZero();
	}

	//输出处理
	pcl::ExtractIndices<pcl::PointXYZ> extract;//由于SACSegmentation抽出来的只是点的数值，需要将数值转化为点云。这个类便是专门来干这个的。
	extract.setInputCloud(Source);
	extract.setIndices(inliers);
	extract.setNegative(false);
	// Get the points associated with the planar surface
	extract.filter(*Plane);
	// Remove the planar inliers, extract the rest
	extract.setNegative(true);
	extract.filter(*Rest);

	PCAOut Res = PCA_Cal(Plane);
	Vector6f Ret;
	Ret(0) = Res.Transform[0];  // Center point x
	Ret(1) = Res.Transform[1];  // Center point y
	Ret(2) = Res.Transform[2];  // Center point z
	Ret(3) = coefficients->values[0];  // Direction x
	Ret(4) = coefficients->values[1];  // Direction y
	Ret(5) = coefficients->values[2];  // Direction z

	return Ret;
}
// ExtractPlane: Percent limit version
/**
 * @brief Extract the plane with max number of points.
 * 
 * @param Source Oringin point cloud pointer.
 * @param Plane The plane extracted.
 * @param Rest The rest of points.
 * @param ThreadHold The thinkness of plane.
 * @param Percent The point number of extracted plane must be larger than [point number of Source]
 * @return Vector6f [Center point x, Center point y, Center point z, Normal x, Normal y, Normal z]
 */
Vector6f ExtractPlane(PCXYZ_Ptr Source, PCXYZ_Ptr Plane, PCXYZ_Ptr Rest, float ThreadHold, double Percent)//抽出平面，Rest去除平面后的点云；返回值是抽出的平面。
{
	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
	// Create the segmentation object
	pcl::SACSegmentation<pcl::PointXYZ> seg;
	// Optional
	seg.setOptimizeCoefficients(true);
	// Mandatory
	seg.setModelType(pcl::SACMODEL_PLANE);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setDistanceThreshold(ThreadHold);//0.02

	seg.setInputCloud(Source);
	seg.segment(*inliers, *coefficients);
	if (inliers->indices.size() <= (*Source).size() * Percent)// 控制最小点数
	{
		PCL_ERROR("ExtractPlane(): Could not estimate a planar model for the given dataset.");
		Plane->clear();
		Rest->clear();
		return Vector6f().setZero();
	}

	//输出处理
	pcl::ExtractIndices<pcl::PointXYZ> extract;//由于SACSegmentation抽出来的只是点的数值，需要将数值转化为点云。这个类便是专门来干这个的。
	extract.setInputCloud(Source);
	extract.setIndices(inliers);
	extract.setNegative(false);
	// Get the points associated with the planar surface
	extract.filter(*Plane);
	// Remove the planar inliers, extract the rest
	extract.setNegative(true);
	extract.filter(*Rest);

	PCAOut Res = PCA_Cal(Plane);
	Vector6f Ret;
	Ret(0) = Res.Transform[0];  // Center point x
	Ret(1) = Res.Transform[1];  // Center point y
	Ret(2) = Res.Transform[2];  // Center point z
	Ret(3) = coefficients->values[0];  // Direction x
	Ret(4) = coefficients->values[1];  // Direction y
	Ret(5) = coefficients->values[2];  // Direction z

	return Ret;
}

// ExtractPlane: Points limit version
/**
 * @brief (NOT WORKED!!!)Extract the plane with same normal specified by [Axis].
 * 
 * @param Source Oringin point cloud pointer.
 * @param Axis The direction of plane that you want to extract.
 * @param Plane The plane extracted.
 * @param Rest The rest of points.
 * @param ThreadHold The thinkness of plane.
 * @param MinPoints The point number of extracted plane must be larger than [MinPoints]. If not, return empty or zeros.
 * @return Vector6f [Center point x, Center point y, Center point z, Normal x, Normal y, Normal z]
 */
Vector6f ExtractParallelPlane(PCXYZ_Ptr Source, Vec3f Axis, PCXYZ_Ptr Plane, PCXYZ_Ptr Rest, float ThreadHold, int MinPoints)//抽出平面，Rest去除平面后的点云；返回值是抽出的平面。
{
	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
	// Create the segmentation object
	pcl::SACSegmentation<pcl::PointXYZ> seg;
	// Optional
	seg.setOptimizeCoefficients(true);
	// Mandatory
	seg.setAxis(Axis);
	seg.setOptimizeCoefficients(false);
	seg.setEpsAngle(  10.0f * TO_RAD);
	seg.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setDistanceThreshold(ThreadHold);//0.02

	seg.setInputCloud(Source);
	seg.segment(*inliers, *coefficients);

	if (inliers->indices.size() <= (unsigned int)MinPoints)// 控制最小点数
	{
		PCL_ERROR("ExtractPlane(): Could not estimate a planar model for the given dataset.");
		Plane->clear();
		Rest->clear();
		return Vector6f().setZero();
	}

	//输出处理
	pcl::ExtractIndices<pcl::PointXYZ> extract;//由于SACSegmentation抽出来的只是点的数值，需要将数值转化为点云。这个类便是专门来干这个的。
	extract.setInputCloud(Source);
	extract.setIndices(inliers);
	extract.setNegative(false);
	// Get the points associated with the planar surface
	extract.filter(*Plane);
	// Remove the planar inliers, extract the rest
	extract.setNegative(true);
	extract.filter(*Rest);

	PCAOut Res = PCA_Cal(Plane);
	Vector6f Ret;
	Ret(0) = Res.Transform[0];  // Center point x
	Ret(1) = Res.Transform[1];  // Center point y
	Ret(2) = Res.Transform[2];  // Center point z
	Ret(3) = coefficients->values[0];  // Direction x
	Ret(4) = coefficients->values[1];  // Direction y
	Ret(5) = coefficients->values[2];  // Direction z

	return Ret;
}

/**
 * @brief Extract the cylinder shape using RANSAC.
 * 
 * @param Source Oringin point cloud pointer.
 * @param Cylinder The cylinder extracted.
 * @param Rest The rest of points.
 * @param ThreadHold The thinkness of cylinder.
 * @param MinPoints The point number of extracted cylinder must be larger than [MinPoints]. If not, return empty or zeros.
 * @return Vector7f [Center point x, Center point y, Center point z, Direction x, Direction y, Direction z, Radius].
 */
Vector7f ExtractCylinder(PCXYZ_Ptr Source, PCXYZ_Ptr Cylinder, PCXYZ_Ptr Rest, float ThreadHold, double Percent)
{
	pcl::search::KdTree<PointXYZ>::Ptr tree (new pcl::search::KdTree<PointXYZ> ());
	pcl::NormalEstimationOMP<PointXYZ, pcl::Normal> ne;
	pcl::PointCloud<pcl::Normal>::Ptr Normals (new pcl::PointCloud<pcl::Normal>);
	pcl::SACSegmentationFromNormals<PointXYZ, pcl::Normal> seg;

	ne.setSearchMethod (tree);
	ne.setInputCloud (Source);
	ne.setKSearch (15);
	ne.compute (*Normals);

	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
	// Optional
	seg.setOptimizeCoefficients(true);
	// Mandatory
	seg.setModelType (pcl::SACMODEL_CYLINDER);
	seg.setMethodType (pcl::SAC_MLESAC);
	seg.setNormalDistanceWeight (0.1);
	seg.setMaxIterations (10000);
	seg.setDistanceThreshold (ThreadHold);
	seg.setRadiusLimits (0, 0.1);
	seg.setInputCloud (Source);
	seg.setInputNormals (Normals);

	// Obtain the cylinder inliers and coefficients
	seg.segment (*inliers, *coefficients);
	std::cerr << "Cylinder coefficients: " << *coefficients << std::endl;

	if (inliers->indices.size() <= (*Source).size() * Percent)  // 控制最小点数
	{
		PCL_ERROR("Could not estimate a cylinder model for the given dataset.");
		return Vector7f().setZero();
	}

	//输出处理
	pcl::ExtractIndices<pcl::PointXYZ> extract;//由于SACSegmentation抽出来的只是点的数值，需要将数值转化为点云。这个类便是专门来干这个的。
	extract.setInputCloud(Source);
	extract.setIndices(inliers);
	extract.setNegative(false);
	// Get the points associated with the planar surface
	extract.filter(*Cylinder);
	// Remove the planar inliers, extract the rest
	extract.setNegative(true);
	extract.filter(*Rest);
	Vector7f Ret;
	Ret(0) = coefficients->values[0];  // Center point x
	Ret(1) = coefficients->values[1];  // Center point y
	Ret(2) = coefficients->values[2];  // Center point z
	Ret(3) = coefficients->values[3];  // Direction x
	Ret(4) = coefficients->values[4];  // Direction y
	Ret(5) = coefficients->values[5];  // Direction z
	Ret(6) = coefficients->values[6];  // Radius
	return Ret;
}


/**
 * @brief 
 * 
 * @param Source Oringin point cloud pointer.
 * @param Circle The circle extracted.
 * @param Rest The rest of points.
 * @param ThreadHold The thinkness of circle.
 * @param MinPoints The point number of extracted circle must be larger than [MinPoints]. If not, return empty or zeros.
 * @return Vector7f [Center point x, Center point y, Center point z, Direction x, Direction y, Direction z, Radius].
 */
Vector7f ExtractCircle3D(PCXYZ_Ptr Source, PCXYZ_Ptr Circle, PCXYZ_Ptr Rest, float ThreadHold, double Percent)
{
	pcl::search::KdTree<PointXYZ>::Ptr tree (new pcl::search::KdTree<PointXYZ> ());
	pcl::NormalEstimationOMP<PointXYZ, pcl::Normal> ne;
	pcl::PointCloud<pcl::Normal>::Ptr Normals (new pcl::PointCloud<pcl::Normal>);
	pcl::SACSegmentationFromNormals<PointXYZ, pcl::Normal> seg;

	ne.setSearchMethod (tree);
	ne.setInputCloud (Source);
	ne.setKSearch (50);
	ne.compute (*Normals);

	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
	// Optional
	seg.setOptimizeCoefficients(true);
	// Mandatory
	seg.setModelType (pcl::SACMODEL_CIRCLE3D);
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setNormalDistanceWeight (0.1);
	seg.setMaxIterations (10000);
	seg.setDistanceThreshold (ThreadHold);
	seg.setRadiusLimits (0, 0.1);
	seg.setInputCloud (Source);
	seg.setInputNormals (Normals);

	// Obtain the cylinder inliers and coefficients
	seg.segment (*inliers, *coefficients);
	std::cerr << "Cylinder coefficients: " << *coefficients << std::endl;

	if (inliers->indices.size() <= (*Source).size() * Percent)  // 控制最小点数
	{
		PCL_ERROR("Could not estimate a cylinder model for the given dataset.");
		return Vector7f().setZero();
	}

	//输出处理
	pcl::ExtractIndices<pcl::PointXYZ> extract;//由于SACSegmentation抽出来的只是点的数值，需要将数值转化为点云。这个类便是专门来干这个的。
	extract.setInputCloud(Source);
	extract.setIndices(inliers);
	extract.setNegative(false);
	// Get the points associated with the planar surface
	extract.filter(*Circle);
	// Remove the planar inliers, extract the rest
	extract.setNegative(true);
	extract.filter(*Rest);

	Vector7f Ret;
	Ret(0) = coefficients->values[0];  // Center point x
	Ret(1) = coefficients->values[1];  // Center point y
	Ret(2) = coefficients->values[2];  // Center point z
	Ret(3) = coefficients->values[3];  // Radius
	Ret(4) = coefficients->values[4];  // Direction x
	Ret(5) = coefficients->values[5];  // Direction y
	Ret(6) = coefficients->values[6];  // Direction z

	return Ret;
}

/**
 * @brief Calculate the rotation matrix (from right hand base coordinate) with Z-Axis point to [Dir], the X-Axis is point to the cross product of [Ref] and [Dir]
 * todo:
 * @param Dir 
 * @param Ref 
 * @return Mat3f Result in Eigen::Mat3f type.
 */
Mat3f CalRotationMatrix_byDirectionVec(Eigen::Vector3f Dir, Eigen::Vector3f Ref)
{
	Eigen::Vector3f Xaxis = Ref.cross(Dir);
	Xaxis.normalize();
	Eigen::Vector3f Yaxis = Dir.cross(Xaxis);
	Yaxis.normalize();

	Mat3f ret;
	ret(0, 0) =	Xaxis(0);ret(0, 1) = Yaxis(0);ret(0, 2) = Dir(0);
	ret(1, 0) =	Xaxis(1);ret(1, 1) = Yaxis(1);ret(1, 2) = Dir(1);
	ret(2, 0) =	Xaxis(2);ret(2, 1) = Yaxis(2);ret(2, 2) = Dir(2);

	return ret;
}




/**************************************
 * @brief: Smooth the surface with the given point cloud.
 * @param[in]:	PCXYZ_Ptr Source : A pointer to point cloud that you want to smooth.
 * @param[in]:	float SearchRadius : The higher you set, the surface is more flatter.
 * @return:		PCXYZ_Ptr : Smoothed point cloud pointer.
 * @note: 
 * @author: Dong
 * @date: 2019/06/04-20:10:03
 **************************************/
PCXYZ_Ptr ReconstructSurface(PCXYZ_Ptr Source, float SearchRadius)
{
	if((*Source).size() == 0)
		return PCXYZ_Ptr(new PCXYZ);

	// Create a KD-Tree
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);

	// Init reconstruction object (second point type is for the normals, even if unused)
	pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ> mls;

	// Set parameters
	mls.setInputCloud (Source);
	mls.setPolynomialOrder (true);
	mls.setSearchMethod (tree);
	mls.setSearchRadius (SearchRadius);

	// Reconstruct
	PCXYZ_Ptr output(new PCXYZ);
	mls.process(*output);

	return output;
}


/**************************************
 * @brief: (NOT WORKED!!!)Test function. Smooth the surface and fill the hole with the given point cloud.
 * @param[in]:	PCXYZ_Ptr Source : A pointer to point cloud that you want to smooth.
 * @param[in]:	float SearchRadius : The higher you set, the surface is more flatter.
 * @return:		PCXYZ_Ptr : Smoothed point cloud pointer.
 * @note:
 * @author: Dong
 * @date: 2019/06/04-20:27:43
 **************************************/
PCXYZ_Ptr ReconstructSurface_FillHole(PCXYZ_Ptr Source, float SearchRadius)
{
	if((*Source).size() == 0)
		return PCXYZ_Ptr(new PCXYZ);

	// Create a KD-Tree
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);

	// Init reconstruction object (second point type is for the normals, even if unused)
//	pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ> mls;
	pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> mls;

	mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal>::SAMPLE_LOCAL_PLANE);
	mls.setUpsamplingRadius (0.002);
	mls.setUpsamplingStepSize (0.001);
//	mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal>::VOXEL_GRID_DILATION);
//	mls.setDilationIterations(5);
//	mls.setDilationVoxelSize(0.00005);
	mls.setPolynomialOrder(2);
	mls.setComputeNormals (true);

	// Set parameters
	mls.setInputCloud (Source);
//	mls.setPolynomialOrder (true);
	mls.setSearchMethod (tree);
	mls.setSearchRadius (SearchRadius);

	// Reconstruct
	PCXYZN_Ptr out(new PCXYZN);
	mls.process(*out);
	PCXYZ_Ptr output(new PCXYZ);
	output = Cvt_PCXYZN_To_PCXYZ(out);

	return output;
}


/**
 * @brief Clustering using euclidean distance.
 * 
 * @param Source Oringin point cloud.
 * @param Clusters The array of result of clustering.
 * @param Setting Settings of euclidean cluster.
 * @param ClusterWithoutZAxis Project points to XoY plane when clustering.
 * @note We can not get complete point cloud of object on the table. If we just use 3D clustering, it may be seperate the object to many pieces. To avoid this issue, you need to use [ClusterWithoutZAxis] with [true].
 * @return int Number of clusters.
 */
int ExtractEuclideanCluster(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[], EC_Setting Setting, bool ClusterWithoutZAxis)
{
	PCXYZ_Ptr NoZ = PCXYZ_Ptr(new PCXYZ);
	if(ClusterWithoutZAxis)
	{
		for (PointXYZ point : *Source)
		{
			point.z = 0;
			NoZ->points.push_back(point);
		}
	}

	// Creating the KdTree object for the search method of the extraction
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
	if(ClusterWithoutZAxis)
		tree->setInputCloud(NoZ);
	else
		tree->setInputCloud(Source);

	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
	ec.setClusterTolerance(Setting.ClusterTolerance); // 2cm
	ec.setMinClusterSize(Setting.MinClusterSize);
	ec.setMaxClusterSize(Setting.MaxClusterSize);
	ec.setSearchMethod(tree);
	if(ClusterWithoutZAxis)
		ec.setInputCloud(NoZ);
	else
		ec.setInputCloud(Source);
	ec.extract(cluster_indices);

	int i = 0;
	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it)
	{
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZ>);
		for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
			cloud_cluster->points.push_back(Source->points[*pit]); //*
		cloud_cluster->width = cloud_cluster->points.size();
		cloud_cluster->height = 1;
		cloud_cluster->is_dense = true;
		Clusters[i++] = cloud_cluster;
	}
	return cluster_indices.size();
}


/**
 * @brief Clustering using euclidean distance.
 * 
 * @deprecated
 * @param Source Oringin point cloud.
 * @param Clusters The point cloud pointer array of result of clustering.
 * @param Setting Settings of euclidean cluster.
 * @param ClusterWithoutZAxis Project points to XoY plane when clustering.
 * @note We can not get complete point cloud of object on the table. If we just use 3D clustering, it may be seperate the object to many pieces. To avoid this issue, you need to use [ClusterWithoutZAxis] with [true].
 * @return int Number of clusters.
 */
int ExtractEuclideanCluster(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[])
{
	// Creating the KdTree object for the search method of the extraction
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud(Source);

	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
	ec.setClusterTolerance(0.01); // 2cm
	ec.setMinClusterSize(150);
	ec.setMaxClusterSize(45000);
	ec.setSearchMethod(tree);
	ec.setInputCloud(Source);
	ec.extract(cluster_indices);

	int i = 0;
	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it)
	{
		PCXYZ_Ptr cloud_cluster(new PCXYZ);
		for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
			cloud_cluster->points.push_back(Source->points[*pit]); //*
		cloud_cluster->width = cloud_cluster->points.size();
		cloud_cluster->height = 1;
		cloud_cluster->is_dense = true;
		Clusters[i++] = cloud_cluster;
	}
	return cluster_indices.size();
}


/**
 * @brief Clustering using Region Grow method.
 * Get more details in <a href="http://pointclouds.org/documentation/tutorials/region_growing_segmentation.php#region-growing-segmentation">Region-Growing-Segmentation</a>.
 * @param Source Oringin point cloud.
 * @param Clusters The point cloud pointer array of result of clustering.
 * @param Setting Settings of region grow cluster. {@link RGC_Setting}
 * @return int Number of clusters.
 */
int ExtractRegionGrowCLuster(PCXYZ_Ptr Source, PCXYZ_Ptr Clusters[], RGC_Setting Setting)
{
	pcl::search::Search<PointXYZ>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZ> > (new pcl::search::KdTree<pcl::PointXYZ>);
	pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
	pcl::NormalEstimationOMP<PointXYZ, pcl::Normal> normal_estimator;
	normal_estimator.setSearchMethod (tree);
	normal_estimator.setInputCloud (Source);
	normal_estimator.setKSearch (Setting.NormalKSerach);
	normal_estimator.compute (*normals);

	pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
	reg.setMinClusterSize (Setting.MinClusterSize);
	reg.setMaxClusterSize (Setting.MaxClusterSize);
	reg.setSearchMethod (tree);
	reg.setNumberOfNeighbours (Setting.NumberOfNeighbours);
	reg.setInputCloud (Source);
	reg.setInputNormals (normals);
	reg.setSmoothnessThreshold (Setting.SmoothnessThreshold);
	reg.setCurvatureThreshold (Setting.CurvatureThreshold);

	std::vector <pcl::PointIndices> cluster_indices;
	reg.extract (cluster_indices);

	int i = 0;
	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it)
	{
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZ>);
		for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
			cloud_cluster->points.push_back(Source->points[*pit]); //*
		cloud_cluster->width = cloud_cluster->points.size();
		cloud_cluster->height = 1;
		cloud_cluster->is_dense = true;
		Clusters[i++] = cloud_cluster;
	}

	return cluster_indices.size();
}


/**
 * @brief (I forgot the function of this function)Calculate <a href="http://docs.pointclouds.org/trunk/classpcl_1_1_s_h_o_t_estimation.html">SHOT</a> features in the model and scene.
 * The performance is not good, DO NOT use this function.
 * 
 * @param Model 
 * @param Model_Normal 
 * @param Scene 
 * @param Scene_Normal 
 * @param RadiusSearch 
 * @return pcl::CorrespondencesPtr 
 */
pcl::CorrespondencesPtr FindCorres_WithSHOTFeature(PCXYZ_Ptr Model, PC_Normal_Ptr Model_Normal, PCXYZ_Ptr Scene, PC_Normal_Ptr Scene_Normal, float RadiusSearch)
{
	pcl::PointCloud<DescriptorType>::Ptr model_descriptors (new pcl::PointCloud<DescriptorType> ());
	pcl::PointCloud<DescriptorType>::Ptr scene_descriptors (new pcl::PointCloud<DescriptorType> ());
	//
	//  Compute Descriptor for keypoints
	//
	pcl::SHOTEstimationOMP<PointXYZ, pcl::Normal, DescriptorType> descr_est;
	descr_est.setRadiusSearch (RadiusSearch);

	descr_est.setInputCloud (Model);
	descr_est.setInputNormals (Model_Normal);
	descr_est.setSearchSurface (Model);
	descr_est.compute (*model_descriptors);

	descr_est.setInputCloud (Scene);
	descr_est.setInputNormals (Scene_Normal);
	descr_est.setSearchSurface (Scene);
	descr_est.compute (*scene_descriptors);

	//
	//  Find Model-Scene Correspondences with KdTree
	//
	pcl::CorrespondencesPtr model_scene_corrs (new pcl::Correspondences ());

	pcl::KdTreeFLANN<DescriptorType> match_search;
	match_search.setInputCloud (model_descriptors);

	//  For each scene keypoint descriptor, find nearest neighbor into the model keypoints descriptor cloud and add it to the correspondences vector.
	for (size_t i = 0; i < scene_descriptors->size (); ++i)
	{
		std::vector<int> neigh_indices (1);
		std::vector<float> neigh_sqr_dists (1);
		if (!__finite (scene_descriptors->at (i).descriptor[0])) //skipping NaNs
			continue;
		int found_neighs = match_search.nearestKSearch (scene_descriptors->at (i), 1, neigh_indices, neigh_sqr_dists);
		if(found_neighs == 1 && neigh_sqr_dists[0] < 0.25f) //  add match only if the squared descriptor distance is less than 0.25 (SHOT descriptor distances are between 0 and 1 by design)
		{
			pcl::Correspondence corr (neigh_indices[0], static_cast<int> (i), neigh_sqr_dists[0]);
			model_scene_corrs->push_back (corr);
		}
	}
	std::cout << "Correspondences found: " << model_scene_corrs->size () << std::endl;
	return model_scene_corrs;
}


/**
 * @brief Matching model in the scene using SHOT feature.
 * The performance is not good, DO NOT use this function.
 * 
 * @param Model : This point cloud must be downsampled.
 * @param Scene : This point cloud must be downsampled.
 * @return : Number of recognized PointCloud.
 */
int RecognitionModel(PCXYZ_Ptr Model, PCXYZ_Ptr Scene, stdVector_Mat4f &Tran, std::vector<pcl::Correspondences> &clustered_corrs, RegnSetting Setting)
{
	pcl::PointCloud<pcl::ReferenceFrame>::Ptr Model_RefFrame (new pcl::PointCloud<pcl::ReferenceFrame> ());
	pcl::PointCloud<pcl::ReferenceFrame>::Ptr Scene_RefFrame (new pcl::PointCloud<pcl::ReferenceFrame> ());

	PC_Normal_Ptr Model_Normal(new PC_Normal);
	PC_Normal_Ptr Scene_Normal(new PC_Normal);

	pcl::BOARDLocalReferenceFrameEstimation<PointXYZ, pcl::Normal, pcl::ReferenceFrame> rf_est;
	pcl::Hough3DGrouping<PointXYZ, PointXYZ, pcl::ReferenceFrame, pcl::ReferenceFrame> clusterer;

	pcl::NormalEstimationOMP<PointXYZ, pcl::Normal> norm_est;
	norm_est.setKSearch (Setting.NormalKSearch);
	norm_est.setInputCloud (Model);
	norm_est.compute (*Model_Normal);
	norm_est.setInputCloud (Scene);
	norm_est.compute (*Scene_Normal);

	// Compute Model-Scene Correspondences.
	pcl::CorrespondencesPtr model_scene_corrs = FindCorres_WithSHOTFeature(Model, Model_Normal, Scene, Scene_Normal, 0.01f);

	// Compute (Keypoints) Reference Frames for Hough
	rf_est.setFindHoles (Setting.BOARD_RF_FindHole);
	rf_est.setRadiusSearch (Setting.BOARD_RF_RadiusSearch);

	rf_est.setInputCloud (Model);
	rf_est.setInputNormals (Model_Normal);
	rf_est.setSearchSurface (Model);
	rf_est.compute (*Model_RefFrame);

	rf_est.setInputCloud (Scene);
	rf_est.setInputNormals (Scene_Normal);
	rf_est.setSearchSurface (Scene);
	rf_est.compute (*Scene_RefFrame);

	// Clustering
	clusterer.setHoughBinSize (Setting.HoughBinSize);
	clusterer.setHoughThreshold (Setting.HoughThreshold);
	clusterer.setUseInterpolation (Setting.HoughUseInterpolation);
	clusterer.setUseDistanceWeight (Setting.HoughUseDistanceWeight);

	clusterer.setInputCloud (Model);
	clusterer.setInputRf (Model_RefFrame);
	clusterer.setSceneCloud (Scene);
	clusterer.setSceneRf (Scene_RefFrame);
	clusterer.setModelSceneCorrespondences (model_scene_corrs);

	clusterer.recognize (Tran, clustered_corrs);

	std::cout << "RecognitionModel(): Model instances found: " << Tran.size () << std::endl;
	for (size_t i = 0; i < Tran.size (); ++i)
	{
		std::cout << "\nRecognitionModel(): Instance " << i + 1 << ":" << std::endl;
		std::cout << "        Correspondences belonging to this instance: " << clustered_corrs[i].size () << std::endl;

		// Print the rotation matrix and translation vector
		Eigen::Matrix3f rotation = Tran[i].block<3,3>(0, 0);
		Eigen::Vector3f translation = Tran[i].block<3,1>(0, 3);

		printf ("\n");
		printf ("            | %6.3f %6.3f %6.3f | \n", rotation (0,0), rotation (0,1), rotation (0,2));
		printf ("        R = | %6.3f %6.3f %6.3f | \n", rotation (1,0), rotation (1,1), rotation (1,2));
		printf ("            | %6.3f %6.3f %6.3f | \n", rotation (2,0), rotation (2,1), rotation (2,2));
		printf ("\n");
		printf ("        t = < %0.3f, %0.3f, %0.3f >\n", translation (0), translation (1), translation (2));
	}
	return Tran.size();
}


/**
 * @brief ICP function
 * Find [the pose and position of model] in the scene.
 * 
 * @param Source The point cloud will be moved. (Always input the model.)
 * @param Target The point cloud not be moved. (Always input the scene.)
 * @param Output Moved [Source] point cloud.
 * @param Setting Settings of ICP algorithm.
 * @return Mat4f The pose and position of [Source] in [Target] point cloud coordinate.
 */
Mat4f ICP_Single(PCXYZ_Ptr Source, PCXYZ_Ptr Target, PCXYZ_Ptr Output, ICP_Setting Setting)
{
	pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;

	icp.setMaxCorrespondenceDistance(Setting.MaxCorrespondenceDistance);
	icp.setTransformationEpsilon(Setting.TransformationEpsilon);
	icp.setEuclideanFitnessEpsilon(Setting.EuclideanFitnessEpsilon);

	icp.setMaximumIterations(Setting.MaximumIterations);
	icp.setInputSource((*Source).makeShared());
	icp.setInputTarget(Target);//这个点云不动，其他点云跟Target配合。
	icp.align(*Output);

	if (icp.hasConverged())
	{
		std::cout << "ICP has converged, score is " << icp.getFitnessScore() << std::endl;
		if (icp.getFitnessScore() > Setting.ScoreLimit)
		{
			PCL_WARN("\nICP score is too large, calculate faild.\n");
			return Mat4f().setZero();
		}
		Eigen::Matrix4f Tran = icp.getFinalTransformation();
		pcl::transformPointCloud(*Source, *Output, Tran);
		return Tran;
	}
	else
	{
		PCL_ERROR("\nICP has not converged.\n");
		return Mat4f().setZero();
	}
}

/**
 * @brief Old function, DO NOT use.
 * 
 * @deprecated
 * @param Source 
 * @param Target 
 * @param Output 
 * @param ScoreLimit 
 * @return Mat4f 
 */
Mat4f ICP_Single(PCXYZ_Ptr Source, PCXYZ_Ptr Target, PCXYZ_Ptr Output, double ScoreLimit)
{
	pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;

	icp.setMaxCorrespondenceDistance(1);
	icp.setTransformationEpsilon(1e-10);
	icp.setEuclideanFitnessEpsilon(10e-6);

	icp.setMaximumIterations(100);
	icp.setInputSource((*Source).makeShared());
	icp.setInputTarget(Target);//这个点云不动，其他点云跟Target配合。
	icp.align(*Output);

	if (icp.hasConverged())
	{
		std::cout << "ICP has converged, score is " << icp.getFitnessScore() << std::endl;
		if (icp.getFitnessScore() > ScoreLimit)
		{
			PCL_WARN("ICP score is too large, calculate faild.\n");
			return Mat4f().setZero();
		}
		Eigen::Matrix4f Tran = icp.getFinalTransformation();
		pcl::transformPointCloud(*Source, *Output, Tran);
		return Tran;
	}
	else
	{
		PCL_ERROR("\nICP has not converged.\n");
		return Mat4f().setZero();
	}
}


// ICP limited version
// If angle or transformation over the limit, this function will not transform input pointcloud.
// AngleLimit is in Degrees. TranLimit is in meter.
/**
 * @brief ICP limited version. If angle or transformation over the limit, this function will not transform input pointcloud. AngleLimit is in Degrees. TranLimit is in meter.
 * 
 * @param Source 
 * @param Target 
 * @param Output 
 * @param Setting 
 * @return Mat4f 
 */
Mat4f ICP_Single_Limit(PCXYZ_Ptr Source, PCXYZ_Ptr Target, PCXYZ_Ptr &Output, ICP_Setting Setting)
{
	pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;

	icp.setMaxCorrespondenceDistance(Setting.MaxCorrespondenceDistance);
	icp.setTransformationEpsilon(Setting.TransformationEpsilon);
	icp.setEuclideanFitnessEpsilon(Setting.EuclideanFitnessEpsilon);

	icp.setMaximumIterations(Setting.MaximumIterations);
	icp.setInputSource((*Source).makeShared());
	icp.setInputTarget(Target);//这个点云不动，其他点云跟Target配合。
	icp.align(*Output);

	if (icp.hasConverged())
	{
		std::cout << "ICP has converged, score is " << icp.getFitnessScore() << ".";

		if (icp.getFitnessScore() > Setting.ScoreLimit)
		{
			PCL_WARN("\nICP score is too large, calculate faild.\n");
			Output.get()->clear();
			Output = (Source).get()->makeShared();
			return Mat4f().setZero();
		}

		Eigen::AngleAxisf angle;
		Eigen::Matrix4f Tran = icp.getFinalTransformation();
		angle.fromRotationMatrix(Tran.block<3,3>(0,0));
		std::cout.precision(7);
		double Distance = Tran(0,3)*Tran(0,3) + Tran(1,3)*Tran(1,3) + Tran(2,3)*Tran(2,3);
		std::cout << " Angle is " << angle.angle() * TO_DEG << ". Translation distance is " << Distance << std::endl;

		if(angle.angle() * TO_DEG > Setting.AngleLimit)
		{
			std::cout << "ICP angle is over the limit, discard the transform." << std::endl;
			Output.get()->clear();
			Output = (Source).get()->makeShared();
			return Tran;
		}
		if (Distance > Setting.TranLimit * Setting.TranLimit)
		{
			std::cout << "ICP translation is over the limit, discard the transform." << std::endl;
			Output.get()->clear();
			Output = (Source).get()->makeShared();
			return Tran;
		}
		pcl::transformPointCloud(*Source, *Output, Tran);
		return Tran;
	}
	else
	{
		PCL_ERROR("\nICP has not converged.\n");
		return Mat4f().setZero();
	}
}


/**
 * @brief Calculate the third axis direction using cross product. This function intend to make a coordinate axis vector.
 * 
 * @param LeftVec Left vector in cross product.
 * @param RightVec Right vector in cross product
 * @param isRightHandCoordinate If false the LEFT hand coordinate will be calculated as result and vice versa.
 * @return Vec3f Result vector with Eigen::Vec3f type.
 */
inline Vec3f Cal_ThirdAxisDir(Vec3f LeftVec, Vec3f RightVec, bool isRightHandCoordinate)
{
	if(isRightHandCoordinate)
		return LeftVec.cross(RightVec);
	else
		return -LeftVec.cross(RightVec);
}


/**
 * @brief This function be used for {@link PCA_Cal} function. It intend to correct the rotation matrix that be calculated by {@link PCA_Cal} function with specified axis order and . 
 * 
 * @param Data The rotation matrix that you want correct.
 * @param Priciple1Axis Direction of first. Use number with [-3, -2, -1, 1, 2, 3] to [-z, -y, -x, x, y, z]
 * @param Priciple2Axis Direction of first. Use number with [-3, -2, -1, 1, 2, 3] to [-z, -y, -x, x, y, z]
 * @param isRightHandCoordinate 
 * @param SecondPrincpleAngle NOT USE!!!
 */
void Change_PCA_CoordinateOrder_CoordinateType(Mat4f &Data, int Priciple1Axis, int Priciple2Axis, bool isRightHandCoordinate, float SecondPrincpleAngle)
{
	int Priciple1AxisPos = abs(Priciple1Axis);
	int Priciple2AxisPos = abs(Priciple2Axis);

	assert((Priciple1Axis >= -3 && Priciple1Axis <= 3 && Priciple1Axis != 0) && "In function [Change_PCA_CoordinateOrder_CoordinateType]. Argument [Priciple1Axis] input the invalid value!");
	assert((Priciple2Axis >= -3 && Priciple2Axis <= 3 && Priciple2Axis != 0) && "In function [Change_PCA_CoordinateOrder_CoordinateType]. Argument [Priciple2Axis] input the invalid value!");
	assert((Priciple1Axis != Priciple2Axis) && "In function [Change_PCA_CoordinateOrder_CoordinateType]. Argument [Priciple2Axis] can't equal to [Priciple1Axis]!");

	Vec3f Priciple1 = Data.block<3, 1>(0, 0);
	Vec3f Priciple2 = Data.block<3, 1>(0, 1);
	Vec3f Priciple3 = Data.block<3, 1>(0, 2);

	// Reform matrix to axis specific by users.
	Data.block<3, 1>(0, Priciple1AxisPos - 1) = Priciple1;
	Data.block<3, 1>(0, Priciple2AxisPos - 1) = Priciple2;
	Data.block<3, 1>(0, 6 - Priciple1AxisPos - Priciple2AxisPos - 1) = Priciple3;

	// Create axis vectors into array.
	Vec3f PositiveDirs[3];
	PositiveDirs[0] = Vec3f(1,0,0);
	PositiveDirs[1] = Vec3f(0,1,0);
	PositiveDirs[2] = Vec3f(0,0,1);
	// Fix direction of first priciple.
	// if signal of Priciple1Axis and signal of dot product (PositiveDirs and priciple direction) is not same, inverse the direction.
	if(Priciple1.dot(PositiveDirs[Priciple1AxisPos - 1]) * Priciple1Axis < 0)
		Data.block<3, 1>(0, Priciple1AxisPos - 1) = -Priciple1;
	if(Priciple2.dot(PositiveDirs[Priciple2AxisPos - 1]) * Priciple2Axis < 0)
		Data.block<3, 1>(0, Priciple2AxisPos - 1) = -Priciple2;

	Vec3f Axis_X = Data.block<3, 1>(0, 0);
	Vec3f Axis_Y = Data.block<3, 1>(0, 1);
	Vec3f Axis_Z = Data.block<3, 1>(0, 2);
	// Correct the type of coordinate (Left hand coordinate or right hand coordinate).
	switch(Priciple1AxisPos + Priciple2AxisPos)
	{
		case 1+2:  // x and y.
		{
			Data.block<3, 1>(0, 2) = Cal_ThirdAxisDir(Axis_X, Axis_Y, isRightHandCoordinate);
			break;
		}
		case 1+3:  // x and z.
		{
			Data.block<3, 1>(0, 1) = Cal_ThirdAxisDir(Axis_Z, Axis_X, isRightHandCoordinate);
			break;
		}
		case 2+3:  // y and z.
		{
			Data.block<3, 1>(0, 0) = Cal_ThirdAxisDir(Axis_Y, Axis_Z, isRightHandCoordinate);
			break;
		}
	}
}


/**
 * @brief PCA_Cal: Calculate the PCA(Principle Component Analysis) of provided point cloud.
 * @param Source: Point cloud you want to calculate.
 * @param CoordinateOrder: (Default: "+x+y")Select axis and direction that fix to first and second Principle Component vector. You should input string value like "+x+y" to specific axis and direction. Direction: "+" means the dot product of this direction and UnitZ(0,0,1) is plus. For example: "-z+y" means
 * @param NeedProjection: (Default: false), If true, PCA will calculate point cloud that transformed into eigenspace into PCAOut.ProjectPC.
 * @param isRightHandCoordinate: (Default: true) Default is right hand coordinate. If false, it'll calculate in left hand coordinate.
 * @return PCAOut structor with all of PCA result.
 */
PCAOut PCA_Cal(PCXYZ_Ptr Source, std::string CoordinateOrder, bool NeedProjection, bool isRightHandCoordinate)
{
	// Main code.
	PCAOut Res;
	Res.ProjectPC = PCXYZ_Ptr(new PCXYZ);
	pcl::PCA<PointXYZ> PCA_Calculator(new pcl::PCA<PointXYZ>);
//	Res.PCA_Calculator = pcl::PCA<PointXYZ>(new pcl::PCA<PointXYZ>);
	PCA_Calculator.setInputCloud(Source);
	// At this moment, PCA calculation is finished. You can call PCA_Calculator.getEigenVectors() to obtain eigenvectors.

	// Result process.
	// Make transform matrix that from eigenspace to world space.
	Eigen::Affine3f tmp(Eigen::Translation3f(PCA_Calculator.getMean().block<3, 1>(0, 0)));
	tmp.rotate(PCA_Calculator.getEigenVectors());

	// Calculate transform matrix and transform part.
	Res.TransformMatrix = tmp.matrix();

	// Process direction and CoordinateOrder specific by user. Deal with right hand coordinate.
	int Priciple1Axis = (-(CoordinateOrder[0] - ',')) * (CoordinateOrder[1] - 'w');  // ',' 's acsii code is between '+' and '-', and '+' is lower.
	int Priciple2Axis = (-(CoordinateOrder[2] - ',')) * (CoordinateOrder[3] - 'w');
	Change_PCA_CoordinateOrder_CoordinateType(Res.TransformMatrix, Priciple1Axis, Priciple2Axis, isRightHandCoordinate, 0);

	// Get rotation and transform part of TransformMatrix.
	Res.Transform = Res.TransformMatrix.block<3, 1>(0, 3);
	Res.Rotation = Res.TransformMatrix.block<3, 3>(0, 0);

	// Calculate xyzabc.
	Res.xyzabc.block<3, 1>(0, 0) = Res.Transform;
	Res.xyzabc.block<3, 1>(3, 0) = Res.Rotation.eulerAngles(2, 1, 0);

	if(NeedProjection)
		PCA_Calculator.project(*Source, *Res.ProjectPC);
	return Res;
}

/**
 * @brief Reserve 1 or 2 axis data of points.
 * 
 * @note This function project the point cloud to the XY, YZ, ZX plane or only to x-axis, y-axis, z-axis.
 * For example, XY plane: Coordinate of z in [Source] is change to [Pos1], x and y is not change; XZ plane: Coordinate of y in [Source] is change to [Pos1], x and z is not change. x-axis: Coordinate of x and y is [Pos1] and [Pos2], z is not change.
 * | AxisReserve | x | y | z |
 * | ----------- | ---- | ---- | ---- |
 * | INPUT | OUTPUT |||
 * | "xy" or "yx" | Not Change | Not Change | Pos1 |
 * | "xz" or "zx" | Not Change | Pos1 | Not Change |
 * | "yz" or "zy" | Pos1 | Not Change | Not Change |
 * | "x" | Not Change | Pos1 | Pos2 |
 * | "y" | Pos1 | Not Change | Pos2 |
 * | "z" | Pos1 | Pos2 | Not Change |
 * 
 * @param Source Point cloud you want to project.
 * @param AxisReserve Only accept string in ["xy", "yx", "xz", "zx", "yz", "zy", "x", "y", "z"].
 * @param Pos1 The fixed value assigned to x y or z coordinate
 * @param Pos2 
 * @return PCXYZ_Ptr 
 */
PCXYZ_Ptr CompressPC(PCXYZ_Ptr Source, std::string AxisReserve, float Pos1, float Pos2)
{
	PCXYZ_Ptr tmp = PCXYZ_Ptr(new PCXYZ);
	if (AxisReserve.length() == 3)
		return Source;
	else if (AxisReserve.length() == 0)
		return tmp;

	if (AxisReserve == "x")
	{
		for(PointXYZ point : Source->points)
		{
			point.y = Pos1;
			point.z = Pos2;
			tmp->points.push_back(point);
		}
	}
	else if (AxisReserve == "y")
	{
		for(PointXYZ point : Source->points)
		{
			point.x = Pos1;
			point.z = Pos2;
			tmp->points.push_back(point);
		}
	}
	else if (AxisReserve == "z")
	{
		for(PointXYZ point : Source->points)
		{
			point.x = Pos1;
			point.y = Pos2;
			tmp->points.push_back(point);
		}
	}
	else if (AxisReserve == "xy" || AxisReserve == "yx")
	{
		for(PointXYZ point : Source->points)
		{
			point.z = Pos1;
			tmp->points.push_back(point);
		}
	}
	else if (AxisReserve == "xz" || AxisReserve == "zx")
	{
		for(PointXYZ point : Source->points)
		{
			point.y = Pos1;
			tmp->points.push_back(point);
		}
	}
	else if (AxisReserve == "zy" || AxisReserve == "yz")
	{
		for(PointXYZ point : Source->points)
		{
			point.x = Pos1;
			tmp->points.push_back(point);
		}
	}
	return tmp;
}


/**
 * @brief Cal_AABB: AABB:Axis Aligned Bounding Box. Calculate bounding box that fixed with XYZ axis.
 * @param Source: Point cloud you want to calculate.
 * @param min: Reference storage XYZ axis max data.
 * @param max: Reference storage XYZ axis min data.
 * Output: Two points that XYZ axis max or min data had been written into.
 */
void Cal_AABB(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max)
{
	//pcl::MomentOfInertiaEstimation <pcl::PointXYZ> feature_extractor;
	//feature_extractor.setInputCloud (Source);
	//feature_extractor.compute ();
	//feature_extractor.getAABB (min, max);
	min.x = INFINITY;
	min.y = INFINITY;
	min.z = INFINITY;
	max.x = -INFINITY;
	max.y = -INFINITY;
	max.z = -INFINITY;
	for (PointXYZ point : Source->points)
	{
		if (point.x > max.x)
			max.x = point.x;
		if (point.x < min.x)
			min.x = point.x;

		if (point.y > max.y)
			max.y = point.y;
		if (point.y < min.y)
			min.y = point.y;

		if (point.z > max.z)
			max.z = point.z;
		if (point.z < min.z)
			min.z = point.z;
	}
}
Vec6f Cal_AABB(PCXYZ_Ptr Source)
{
	Vec6f res;
	PointXYZ min, max;
	min.x = INFINITY;
	min.y = INFINITY;
	min.z = INFINITY;
	max.x = -INFINITY;
	max.y = -INFINITY;
	max.z = -INFINITY;
	for (PointXYZ point : Source->points)
	{
		if (point.x > max.x)
			max.x = point.x;
		if (point.x < min.x)
			min.x = point.x;

		if (point.y > max.y)
			max.y = point.y;
		if (point.y < min.y)
			min.y = point.y;

		if (point.z > max.z)
			max.z = point.z;
		if (point.z < min.z)
			min.z = point.z;
	}
	res(0) = min.x;
	res(1) = min.y;
	res(2) = min.z;
	res(3) = max.x;
	res(4) = max.y;
	res(5) = max.z;
	return res;
}

void Get_minPoint(PCXYZ_Ptr Source, PointXYZ& min, std::string axis)
{
	min.x = INFINITY;
	min.y = INFINITY;
	min.z = INFINITY;

	if (axis == "x")
	{
		for (PointXYZ point : Source->points)
		{
			if (point.x < min.x)
			{
				min.x = point.x;
				min.y = point.y;
				min.z = point.z;
			}
		}
	}
	if (axis == "y")
	{
		for (PointXYZ point : Source->points)
		{
			if (point.y < min.y)
			{
				min.x = point.x;
				min.y = point.y;
				min.z = point.z;
			}
		}
	}
	if (axis == "z")
	{
		for (PointXYZ point : Source->points)
		{
			if (point.z < min.z)
			{
				min.x = point.x;
				min.y = point.y;
				min.z = point.z;
			}
		}
	}
}

Eigen::Vector4f Analyze_Dir_Center(PCXYZ_Ptr Source, Mat4f PCA_Result_Matrix)
{
	Eigen::Vector4f Res;
	PCXYZ_Ptr TranOut = PCXYZ_Ptr(new PCXYZ);
	PCXYZ_Ptr Big = PCXYZ_Ptr(new PCXYZ);
	pcl::transformPointCloud(*Source, *TranOut, (Mat4f)PCA_Result_Matrix.inverse());//
	float Delta_X = 0.035;//3.5cm

	pcl::MomentOfInertiaEstimation <pcl::PointXYZ> feature_extractor;
	feature_extractor.setInputCloud (TranOut);
	feature_extractor.compute ();

	PointXYZ org_min_point_AABB;
	PointXYZ org_max_point_AABB;
	feature_extractor.getAABB (org_min_point_AABB, org_max_point_AABB);
//	Eigen::Affine3f tmp1(Eigen::Translation3f(
//							org_min_point_AABB.x,
//							org_min_point_AABB.y,
//							org_min_point_AABB.z));
//	Eigen::Affine3f tmp2(Eigen::Translation3f(
//							org_max_point_AABB.x,
//							org_max_point_AABB.y,
//							org_max_point_AABB.z));


	PCXYZ_Ptr PassThrough_min = PCXYZ_Ptr(new PCXYZ);
	pcl::PassThrough<pcl::PointXYZ> PassFilter;
	PassFilter.setFilterFieldName("x");
	PassFilter.setFilterLimits(org_min_point_AABB.x, org_min_point_AABB.x + Delta_X);  // 0.30
	PassFilter.setInputCloud(TranOut);
	PassFilter.filter(*PassThrough_min);
	feature_extractor.setInputCloud (PassThrough_min);
	feature_extractor.compute ();
	PointXYZ min_point_AABB;
	PointXYZ max_point_AABB;
	feature_extractor.getAABB (min_point_AABB, max_point_AABB);
//	PC_Show_Stuck(PassThrough_min, "Little", tmp1, 0.07);

	PCXYZ_Ptr PassThrough_max = PCXYZ_Ptr(new PCXYZ);
	pcl::PassThrough<pcl::PointXYZ> PassFilter2;
	PassFilter2.setFilterFieldName("x");
//	std::cout << min_point_AABB << std::endl;
//	std::cout << max_point_AABB << std::endl;
//	std::cout << max_point_AABB.x - Delta_X << max_point_AABB.x << std::endl;
	PassFilter2.setFilterLimits(org_max_point_AABB.x - Delta_X, org_max_point_AABB.x);  // 0.30
	PassFilter2.setInputCloud(TranOut);
	PassFilter2.filter(*PassThrough_max);
	feature_extractor.setInputCloud (PassThrough_max);
	feature_extractor.compute ();
	PointXYZ other_min_point_AABB;
	PointXYZ other_max_point_AABB;
	feature_extractor.getAABB (other_min_point_AABB, other_max_point_AABB);

	pcl::PCA<PointXYZ> PCA_Calculator(new pcl::PCA<PointXYZ>);
//	std::cout << fabs(min_point_AABB.y - min_point_AABB.y) << std::endl;
//	std::cout << fabs(other_min_point_AABB.y - other_max_point_AABB.y) << std::endl;
	if(fabs(min_point_AABB.y - max_point_AABB.y) < fabs(other_min_point_AABB.y - other_max_point_AABB.y))
		PCA_Calculator.setInputCloud(PassThrough_max);
	else
		PCA_Calculator.setInputCloud(PassThrough_min);

	Eigen::Affine3f Transform(Eigen::Translation3f(
							PCA_Calculator.getMean().x(),
							PCA_Calculator.getMean().y(),
							PCA_Calculator.getMean().z()));
	Transform.rotate(PCA_Calculator.getEigenVectors());

	// PC_Show_Stuck(TranOut, "BigCenter", Cvt_RotationOrder_XYZ2ZXY(Transform), 0.007);
Std_Out(PCA_Calculator.getEigenVectors().eulerAngles(0, 1, 2));
	Res[0] = PCA_Calculator.getMean().x();
	Res[1] = PCA_Calculator.getMean().y();
	Res[2] = PCA_Calculator.getMean().z();
	Res[3] = 1;

	Eigen::Vector4f Res2 = PCA_Result_Matrix * Res;
	std::cout << Res2 << std::endl;
	return Res2;
}


void RemovePoints_byModelAABB(PCXYZ_Ptr Source, PCXYZ_Ptr Model, PCXYZ_Ptr Output, double Scale)
{
	PointXYZ min, max, mid, diff;
	pcl::MomentOfInertiaEstimation <pcl::PointXYZ> feature_extractor;
	feature_extractor.setInputCloud (Model);
	feature_extractor.compute ();
	feature_extractor.getAABB (min, max);

	mid.x = (max.x + min.x) / 2.0f;
	mid.y = (max.y + min.y) / 2.0f;
	mid.z = (max.z + min.z) / 2.0f;

	diff.x = (max.x - mid.x) * Scale;
	diff.y = (max.y - mid.y) * Scale;
	diff.z = (max.z - mid.z) * Scale;

	PCXYZ_Ptr tmp1 = PCXYZ_Ptr(new PCXYZ);
	PCXYZ_Ptr tmp2 = PCXYZ_Ptr(new PCXYZ);

	pcl::PassThrough<pcl::PointXYZ> PassFilter;
	PassFilter.setFilterFieldName("x");
	PassFilter.setFilterLimits(mid.x - diff.x, mid.x + diff.x);
	PassFilter.setInputCloud(Source);
	PassFilter.filter(*tmp1);

	PassFilter.setFilterFieldName("y");
	PassFilter.setFilterLimits(mid.y - diff.y, mid.y + diff.y);
	PassFilter.setInputCloud(tmp1);
	PassFilter.filter(*tmp2);

	PassFilter.setFilterFieldName("z");
	PassFilter.setFilterLimits(mid.z - diff.z, mid.z + diff.z);
	PassFilter.setInputCloud(tmp2);
	PassFilter.filter(*Output);
}


void RemovePoints_byAABB(PCXYZ_Ptr Source, PointXYZ &min, PointXYZ &max, PCXYZ_Ptr Output, double Scale)
{
	PointXYZ mid, diff;

	mid.x = (max.x + min.x) / 2.0f;
	mid.y = (max.y + min.y) / 2.0f;
	mid.z = (max.z + min.z) / 2.0f;

	diff.x = (max.x - mid.x) * Scale;
	diff.y = (max.y - mid.y) * Scale;
	diff.z = (max.z - mid.z) * Scale;

//Std_Out("min:", min);
//Std_Out("max:", max);
//Std_Out("mid:", mid);
//Std_Out("diff:", diff);

	PCXYZ_Ptr tmp1 = PCXYZ_Ptr(new PCXYZ);
	PCXYZ_Ptr tmp2 = PCXYZ_Ptr(new PCXYZ);

	pcl::PassThrough<pcl::PointXYZ> PassFilter;
	PassFilter.setFilterFieldName("x");
	PassFilter.setFilterLimits(mid.x - diff.x, mid.x + diff.x);
	PassFilter.setInputCloud(Source);
	PassFilter.filter(*tmp1);
//Std_Out("x", mid.x - diff.x, mid.x + diff.x);

	PassFilter.setFilterFieldName("y");
	PassFilter.setFilterLimits(mid.y - diff.y, mid.y + diff.y);
	PassFilter.setInputCloud(tmp1);
	PassFilter.filter(*tmp2);
//Std_Out("y", mid.y - diff.y, mid.y + diff.y);

	PassFilter.setFilterFieldName("z");
	PassFilter.setFilterLimits(mid.z - diff.z, mid.z + diff.z);
	PassFilter.setInputCloud(tmp2);
	PassFilter.filter(*Output);
//Std_Out("z", mid.z - diff.z, mid.z + diff.z);
}


void RemovePoints_byNeartest(PCXYZ_Ptr Source, PCXYZ_Ptr Model, PCXYZ_Ptr OutPut, PCXYZ_Ptr OutPut_Neg, double radius)
{
	pcl::KdTreeFLANN<PointXYZ> kdtree(false);
	kdtree.setInputCloud(Source);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
	std::vector<float> pointRadiusSquaredDistance;

	for(PointXYZ point : Model->points)
	{
		std::vector<int> pointIdxNKNSearch;
		if ( kdtree.radiusSearch (point, radius, pointIdxNKNSearch, pointRadiusSquaredDistance) > 0 )
			inliers->indices.insert(inliers->indices.end(), pointIdxNKNSearch.begin(), pointIdxNKNSearch.end());
	}
	std::sort(inliers->indices.begin(), inliers->indices.end());
	std::vector<int>::iterator it;
	it = std::unique(inliers->indices.begin(), inliers->indices.end());
	inliers->indices.resize( std::distance(inliers->indices.begin(),it));

	pcl::ExtractIndices<pcl::PointXYZ> extract;
	extract.setInputCloud(Source);
	extract.setIndices(inliers);

	extract.setNegative(false);
	extract.filter(*OutPut_Neg);

	extract.setNegative(true);
	extract.filter(*OutPut);
}

std::vector<float> GetNearPoints_Radius(PCXYZ_Ptr Source, PointXYZ Point, PCXYZ_Ptr &OutPut, double radius, bool NeedRest, PCXYZ_Ptr OutPut_Neg)
{
	pcl::KdTreeFLANN<PointXYZ> kdtree(false);
	kdtree.setInputCloud(Source);
	std::vector<float> pointRadiusSquaredDistance;
	pcl::PointIndices::Ptr indices(new pcl::PointIndices);

	if ( kdtree.radiusSearch (Point, radius, indices->indices, pointRadiusSquaredDistance) <= 0 )
	{
		Std_Out("GetNearPoints not found and nearest points!");
		OutPut = PCXYZ_Ptr(new PCXYZ);
		OutPut_Neg = PCXYZ_Ptr(new PCXYZ);
		return pointRadiusSquaredDistance;
	}

	pcl::ExtractIndices<pcl::PointXYZ> extract;
	extract.setInputCloud(Source);
	extract.setIndices(indices);
	extract.setNegative(false);
	// Get the points without model.
	extract.filter(*OutPut);
	if (NeedRest)
	{
		// Extract the rest
		extract.setNegative(true);
		extract.filter(*OutPut_Neg);
	}
	return pointRadiusSquaredDistance;
}


std::vector<float> GetNearPoints_NearestNum(PCXYZ_Ptr Source, PointXYZ Point, PCXYZ_Ptr &OutPut, int NearestNum, bool NeedRest, PCXYZ_Ptr OutPut_Neg)
{
	pcl::KdTreeFLANN<PointXYZ> kdtree(false);
	kdtree.setInputCloud(Source);
	std::vector<float> pointRadiusSquaredDistance;
	pcl::PointIndices::Ptr indices(new pcl::PointIndices);

	if ( kdtree.nearestKSearchT(Point, NearestNum, indices->indices, pointRadiusSquaredDistance) <= 0 )
	{
		Std_Out("GetNearPoints not found and nearest points!");
		OutPut = PCXYZ_Ptr(new PCXYZ);
		OutPut_Neg = PCXYZ_Ptr(new PCXYZ);
		return pointRadiusSquaredDistance;
	}

	pcl::ExtractIndices<pcl::PointXYZ> extract;
	extract.setInputCloud(Source);
	extract.setIndices(indices);
	extract.setNegative(false);
	// Get the points without model.
	extract.filter(*OutPut);
	if (NeedRest)
	{
		// Extract the rest
		extract.setNegative(true);
		extract.filter(*OutPut_Neg);
	}
	return pointRadiusSquaredDistance;
}

void GetPoints_Near2PointsLine(PCXYZ_Ptr Source, PointXYZ P1, PointXYZ P2, PCXYZ_Ptr &OutPut, double radius, double delta)
{
	float Distance = CalDistance(P2, P1);
	int LoopTimes = Distance / delta;
	PointXYZ LineDirectVector = (P2 - P1) / Distance;
	PointXYZ Diff = LineDirectVector * delta;
	PointXYZ Cur(P1), Next(0, 0, 0);
	PCXYZ_Ptr Res(new PCXYZ);
	PCXYZ_Ptr tmp(new PCXYZ);
	PCXYZ_Ptr Model(new PCXYZ);

	// Prepare points of serach center for pickup.
	for(int i = 0; i < LoopTimes; i++)
	{
		Model->points.push_back(Cur);
		Cur = Cur + Diff;
//		Std_Out("Search points: ", Cur);
	}
	if(Cur != P2)
		Model->points.push_back(P2);

	// Decent number of points.
	PCXYZ_Ptr Decent(new PCXYZ);
	GetNearPoints_Radius(Source, (P2 + P1) / 2.0f, Decent, Distance / 2 * 1.5f);

	// Pickup points.
	pcl::KdTreeFLANN<PointXYZ> kdtree(false);
	kdtree.setInputCloud(Decent);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
	std::vector<float> pointRadiusSquaredDistance;

	for(PointXYZ point : Model->points)
	{
		std::vector<int> pointIdxNKNSearch;
		if ( kdtree.radiusSearch (point, radius, pointIdxNKNSearch, pointRadiusSquaredDistance) > 0 )
			inliers->indices.insert(inliers->indices.end(), pointIdxNKNSearch.begin(), pointIdxNKNSearch.end());
	}
	std::sort(inliers->indices.begin(), inliers->indices.end());
	std::vector<int>::iterator it;
	it = std::unique(inliers->indices.begin(), inliers->indices.end());
	inliers->indices.resize( std::distance(inliers->indices.begin(),it));

	pcl::ExtractIndices<pcl::PointXYZ> extract;
	extract.setInputCloud(Decent);
	extract.setIndices(inliers);
	extract.setNegative(false);
	// Get the points without model.
	extract.filter(*OutPut);

//	PCXYZ_Ptr tmp1[3] = {Source, Decent, OutPut};
//	PCShow(tmp1, "Project test.", 3);
//	Std_Out("Found points: ", OutPut->points.size());
}




PlyMesh_Ptr CreatePlyMesh(PCXYZ_Ptr Data)
{
	PlyMesh_Ptr Mesh(new PlyMesh);

	// Normal estimation*
	pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> n;
	pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud (Data);
	n.setInputCloud (Data);
	n.setSearchMethod (tree);
	n.setKSearch (20);
	n.compute (*normals);
	//* normals should not contain the point normals + surface curvatures

	// Concatenate the XYZ and normal fields*
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
	pcl::concatenateFields (*Data, *normals, *cloud_with_normals);
	//* cloud_with_normals = cloud + normals

	// Create search tree*
	pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>);
	tree2->setInputCloud (cloud_with_normals);

	// Initialize objects
	pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
	PlyMesh triangles;

	// Set the maximum distance between connected points (maximum edge length)
	gp3.setSearchRadius (0.025);

	// Set typical values for the parameters
	gp3.setMu (2.5);
	gp3.setMaximumNearestNeighbors (100);
	gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
	gp3.setMinimumAngle(M_PI/18); // 10 degrees
	gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
	gp3.setNormalConsistency(false);

	// Get result
	gp3.setInputCloud (cloud_with_normals);
	gp3.setSearchMethod (tree2);
	gp3.reconstruct (*Mesh);

	return Mesh;
}


int SlicePointCloud(PCXYZ_Ptr Data, PCXYZ_Ptr SlicedPC[], PC_Slice res[], float Thickness, int MinPoints)
{
	PointXYZ min, max;
	Cal_AABB(Data, min, max);

	float StartCut = min.z;
	PCXYZ_Ptr NotEnough (new PCXYZ);
	PCXYZ_Ptr PassOut (new PCXYZ);
	pcl::PassThrough<PointXYZ> PassFilter;
	PassFilter.setFilterFieldName("z");
	int i = 0;

	for(; StartCut < max.z; )
	{
		PassFilter.setFilterLimits(StartCut, StartCut + Thickness);
		PassFilter.setInputCloud(Data);

		PassFilter.filter(*PassOut);

		StartCut += Thickness;
		res[i].Thickness += Thickness;  // Accumelate thickness when number of points of current slice is not enough.
		*NotEnough += *PassOut;

		if(NotEnough->size() < MinPoints)
			continue;

		res[i].StartHeight = StartCut - res[i].Thickness;

		res[i].Sliced_PC = CompressPC(NotEnough, "xy", res[i].StartHeight);

		SlicedPC[i] = NotEnough;

		NotEnough = PCXYZ_Ptr(new PCXYZ);
		i++;
	}
	if(NotEnough->size() > 0)
	{
		res[i].StartHeight = StartCut - res[i].Thickness;
		res[i].Sliced_PC = CompressPC(NotEnough, "xy", res[i].StartHeight);
		SlicedPC[i] = NotEnough;
		return i + 1;
	}
	return i;
}
int SlicePointCloud2(PCXYZ_Ptr Data, PCXYZ_Ptr SlicedPC[], PC_Slice res[], float Thickness, int MinPoints, float CutoffHeight)
{
	PointXYZ min, max;
	Cal_AABB(Data, min, max);
	if (CutoffHeight != INFINITY && (CutoffHeight - min.z > -1e-4 && CutoffHeight - max.z < 1e-4))
		max.z = CutoffHeight;

	float StartCut = min.z;
	PCXYZ_Ptr SliceTmp (new PCXYZ(*Data));

	double minz_d = min.z, maxz_d = max.z, Thickness_d = Thickness;  // For calculate length of result(use double we can get more precise).
	uint_least32_t Num = static_cast<uint_least32_t>(ceil((maxz_d - minz_d) / Thickness_d));
	if (Num == 0)
		return 0;

	std::vector<PCXYZ_Ptr> SlicedPC_tmp(Num);
	for (int i = 0; i < Num; i++)
		SlicedPC_tmp[i] = PCXYZ_Ptr(new PCXYZ);
	std::vector<PC_Slice> res_tmp(Num);

	// First slice.
	std::vector<float> LayerHeightArray(Num, -INFINITY);
	for(PointXYZ point : SliceTmp->points)
	{
//		float slice_axis = point.z;
		if (point.z >= max.z)
			continue;
		uint_least32_t layer_idx = ceil((point.z - StartCut) / Thickness);  // Get the smallest integer that larger than given float number.
		if (layer_idx > 0)
			layer_idx--;
		if(LayerHeightArray[layer_idx] < -10)
			LayerHeightArray[layer_idx] = layer_idx * Thickness + StartCut;

		res_tmp[layer_idx].Sliced_PC->points.push_back(point);
		res_tmp[layer_idx].StartHeight = LayerHeightArray[layer_idx];
		point.z = LayerHeightArray[layer_idx];
		//SlicedPC_tmp[layer_idx] = SlicedPC[layer_idx];
		SlicedPC_tmp[layer_idx]->points.push_back(point);
	}

	for (int i = 0; i < Num; ++i)
	{
		if (LayerHeightArray[i] == -INFINITY)
		{
			LayerHeightArray.erase(LayerHeightArray.begin() + i);
			SlicedPC_tmp.erase(SlicedPC_tmp.begin() + i);
			res_tmp.erase(res_tmp.begin() + i);
			Num--;
		}
	}

	int Valid = 0;
	PCXYZ_Ptr nullPC(new PCXYZ);
	for (int i = 0; i < Num - 1; ++i)  // Last layer is no need to be processed.
	{
		if(SlicedPC_tmp[i]->points.size() < MinPoints)
		{
			for(int j=0; j < SlicedPC_tmp[i]->points.size(); j++)
			{
				SlicedPC_tmp[i]->points[j].z = SlicedPC_tmp[i + 1]->points[0].z;
				SlicedPC_tmp[i + 1]->points.push_back(SlicedPC_tmp[i]->points[j]);

				res_tmp[i + 1].Sliced_PC->push_back(res_tmp[i].Sliced_PC->points[j]);
			}
			res_tmp[i + 1].StartHeight = res_tmp[i].StartHeight;
			res_tmp[i + 1].Thickness += Thickness;
			SlicedPC_tmp[i] = nullPC;
			res_tmp[i].Sliced_PC = nullPC;
		}
		else
		{
			//res_tmp[i].StartHeight += i * Thickness + StartCut;
			res_tmp[i].Thickness += Thickness;
			Valid++;
			continue;
		}
	}
	res_tmp[Num - 1].Thickness = Thickness;
	Valid++;
	int tmpValid = 0;
	int i = 0;
	for (; i < Valid; tmpValid++)  // Last layer is no need to be processed.
	{
		if(SlicedPC_tmp[tmpValid] != nullPC)
		{
			SlicedPC[i] = SlicedPC_tmp[tmpValid];
			res[i] = res_tmp[tmpValid];
			++i;
		}
	}

	return Valid;
}


double VolumeCalculation_byZAxisSlice(PCXYZ_Ptr Model, Vec6f AABB, float LayerHeight)
{
	if (AABB == Vec6f::Zero())
		AABB = Cal_AABB(Model);

	int SegmentCount = (AABB(5) - AABB(2)) / LayerHeight + 10;
	PCXYZ_Ptr* Sliced_Array = new PCXYZ_Ptr[SegmentCount];
	PC_Slice* res = new PC_Slice[SegmentCount];
	float* Area = new float[SegmentCount];
	for (int i = 0; i < SegmentCount; ++i)
	{
		Sliced_Array[i] = PCXYZ_Ptr(new PCXYZ);
		res[i].Sliced_PC = PCXYZ_Ptr(new PCXYZ);
		Area[i] = -1.0f;
	}

	int count = SlicePointCloud2(Model, Sliced_Array, res, LayerHeight, 25, AABB(5));  //
	if (count == 0)
		return 0;
//	PCShow(Sliced_Array, "Sliced", count);
	pcl::ConvexHull<PointXYZ> chull;
	chull.setComputeAreaVolume(true);
	PCXYZ_Ptr cloud_hull(new PCXYZ);
	PCXYZ_Ptr FoundPoint_Org(new PCXYZ);
	double Volume = 0.0;
	for (int i = 0; i < count; i++)
	{
		chull.setInputCloud(Sliced_Array[i]);
		chull.reconstruct(*cloud_hull);
		Area[i] = chull.getTotalArea();
//		Std_Out("==Slice area: ", Area[i]*1e6);
		if (i > 0)
		{
			Volume += (Area[i] + Area[i - 1]) / 2.0 * res[i].Thickness;
//			Volume += (Area[i] ) * res[i].Thickness;
		}
//		Std_Out("==Volume: ", Volume*1e6);
	}
	if (count == 1)
		Volume = Area[0] * res[0].Thickness;
//	Std_Out("==Volume: ", Volume*1e6);
	return Volume;

}


PCXYZ_Ptr TransformPC_viaPoint(PCXYZ_Ptr Data, Vec3f Point, Eigen::Affine3f Rotation)
{
	PCXYZ_Ptr Transd(new PCXYZ);
	PCXYZ_Ptr Transd2(new PCXYZ);
	Eigen::Affine3f Tran2(Eigen::AngleAxisf(0, Vec3f(1, 0, 0)));
	Rotation.translate(-Point);
	Tran2.translate(Point);
	pcl::transformPointCloud(*Data, *Transd, Rotation.matrix());
	pcl::transformPointCloud(*Transd, *Transd2, Tran2.matrix());

	return Transd2;
}
PCXYZ_Ptr TransformPC_viaPoint(PCXYZ_Ptr Data, PointXYZ Point, Eigen::Affine3f Rotation)
{
	Vec3f PointVec = Cvt_PointXYZ2Vec3f(Point);
	return TransformPC_viaPoint(Data, PointVec, Rotation);
}


PCXYZ_Ptr Extract_FromIndices(PCXYZ_Ptr Data, PCIDX_Ptr idx)
{
	PCXYZ_Ptr res(new PCXYZ);
	for (int index : idx->indices)
		res->points.push_back(Data->points[index]);
	return res;
}


Vec6f Cvt_TransformMatrix2xyzabc(Mat4f Data, int RotationOrd1, int RotationOrd2, int RotationOrd3)
{
	Mat3f RotationMat = Data.block<3,3>(0,0);
	Vec3f EulerAngle = RotationMat.eulerAngles(RotationOrd1, RotationOrd2, RotationOrd3);
	Vec3f TranslateVector = Data.block<3,1>(0,3);

	Vec6f Ret;
	Ret(0) = TranslateVector[0];
	Ret(1) = TranslateVector[1];
	Ret(2) = TranslateVector[2];
	Ret(3) = EulerAngle[0];
	Ret(4) = EulerAngle[1];
	Ret(5) = EulerAngle[2];

	return Ret;
}


PCXYZ_Ptr Cvt_PCXYZN_To_PCXYZ(PCXYZN_Ptr Source)
{
	PCXYZ_Ptr out(new PCXYZ);
	PointXYZ tmp;
	for(PointXYZN point : Source->points)
	{
		tmp.x = point.x;
		tmp.y = point.y;
		tmp.z = point.z;
		out->points.push_back(tmp);
	}
	return out;
}


float MeanFilter(int Size, float NewData)
{
	static float* buffer = new float[Size];
	static bool isFirst = true;
	static int FilterSize = Size;
	static float Sum = 0;
	static uint64_t count = 0;
	static int Head = count % FilterSize;

	if(isFirst)
	{
		for(int i = 0; i < Size; i++)
			buffer[i] = 0;
		isFirst = false;
	}

	Head = count % FilterSize;

//	Sum = 0;
//	for(int i = 0; i < size; i++)
//		Sum += buffer[i];

	Sum = Sum - buffer[Head] + NewData;

	Std_Out("buffer[Head]: ", buffer[Head],"NewData: ", NewData, "Sum: ", Sum);
	buffer[Head] = NewData;
	count++;

	return Sum / FilterSize;
}

PointXYZ ProjectPoint2Line(PointXYZ P1, PointXYZ P2, PointXYZ ProjectPoint, float &Percent)
{
	float Distance = CalDistance(P1, P2);
	PointXYZ LineDirectVector = P2 - P1;
	float ProjectLength = ((ProjectPoint - P1) * LineDirectVector) / (Distance);

	Percent = ProjectLength / Distance;
	return P1 + LineDirectVector * Percent;
}

#ifdef __NEED_READ_FROM_FILE__
/// dddd
bool ReadPointCloudData(std::string Path, PCXYZ_Ptr Output)
{
	PCXYZ_Ptr tmp4(new PCXYZ);
	PCXYZ_Ptr tmp5(new PCXYZ);
	pcl::VoxelGrid<pcl::PointXYZ> sor;//新声明体素网格对象
	//sor.setLeafSize(0.001414f, 0.001414f, 0.001414f);//设定体素网格大小
	sor.setLeafSize(0.005f, 0.005f, 0.005f);//设定体素网格大小
	std::mutex ReadingMutex;
	#pragma omp parallel for firstprivate(Output),lastprivate(Output)
	for (int i = 0; i < NUM_OF_DATA; i++)
	{
		PCXYZ_Ptr tmp(new PCXYZ);
		PCXYZ_Ptr tmp2(new PCXYZ);
		PCXYZ_Ptr tmp3(new PCXYZ);
		////D:\\采集数据\\2017-11-23_13-59-40 普通放置   C:\\Users\\Dong\\Documents\\Visual Studio 2017\\Projects\\PCL-重新校准\\x64\\Debug\\tmp
		//("D:\\采集数据\\2017-11-23_14-06-26 带有倾斜\\PointCloud_"
		if (pcl::io::loadPCDFile(Path + std::to_string(i) + ".pcd",	*tmp) < 0)
		{
			std::cout << "Can't load PointCloud_" + std::to_string(i) + ".pcd" << std::endl;
		}
		else
		{
			std::cout << "Load PointCloud_" + std::to_string(i) + ".pcd to Res." << std::endl;
			pcl::transformPointCloud(*tmp, *tmp2, MakeTransformMatrix(TransData[i]));
			Filters(tmp2, tmp3);

			while(!ReadingMutex.try_lock());
			*Output += *tmp3;
			ReadingMutex.unlock();
		}
	}
	tmp4 = Output;
	sor.setInputCloud(tmp4);
	sor.filter(*Output);//过滤点云
	return true;
}
#endif
